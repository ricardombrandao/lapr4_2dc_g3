@startuml
scale 900 width

UIExtensionDataBase -- UIExtension:extends
DataBaseMenu -- JMenu:extends
DataBaseShowResults -- JDialog:extends
DataBaseInfoPane -- JDialog:extends
DataBaseMenu -- DataBaseActionExport 
DataBaseController -- DataBaseFactory
DataBaseActionExport -- FocusOwnerAction:extends
ExtensionDataBase -- Extension:extends

DataBaseController:+RegistarDB()

class DataBaseFactory{
-conn : connection
__
+instance getInstance()
.. Transformação de Strings ..
+ArrayList<String> criaInsert()
+ArrayList<String> colunasTabela()
+String criaTabela()
.. Metodos de BD ..
+void turnDBOn()
+void turnDBOff()
+ResultSet selectQuery()
+void insertQuery()
+boolean checkTableExistence()
+void printQuery()
}

class DataBaseInfoPane{
-Cell[][] : cel
-JTextField : txNomeDB
-JTextField : txUsername
-JPasswordField : txPassword
-JTextField : txTabela
-JTextField : txCells
-JComboBox : comboDB
-UIController : ui
__
-String expressaoCells()
-Char ColumnNumberToChar()
.. Eventos ..
class TrataEventoBtRegistar
class TrataEventoBtCancel
class TrataEventoBtDB
.. Thread ..
class runThread
}

class ExtensionDataBase {
+String NAME
__
+UIExtension getUIExtension()
}

class UIExtensionDataBase{
__
+icon getIcon()
+JMenu getMenu()
+CellDecorator getCellDecorator()
+TableDecorator getTableDecorator()
+ToolBar getToolBar()
+SideBar getSideBar()
}

class DataBaseActionExport{
#UIController : uiController
__
#String getName()
#void defineProperties
}

@enduml