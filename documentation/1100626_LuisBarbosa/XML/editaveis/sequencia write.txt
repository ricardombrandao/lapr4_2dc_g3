title Sequence diagram of write hibernate -> xml with versions

User->CleanSheets: saveAs(Workbook, File)

CleanSheets->CodecFactory: CodecFactory().getCodec(File)

CodecFactory->CodecFactory:getCodec(Extension)

note right of "CodecFactory":NOTE:Procura pelo Extencion + Codec (ex. XMLCodec) nas classes.

CodecFactory-->CleanSheets:codec

alt instanceOf XMLCodec
    CleanSheets->XMLCodec: write(Workbook, Stram, File)

XMLCodec->PersistenceInit: Persistence.Init()

PersistenceInit->Persistence: Perssitence.CreateEntityManagerfactory("CleanSheetsPU")

note right of "PersistenceInit":NOTE:Nome dado no XML de mapeamento Hibernate HSQLDB

PersistenceInit-->XMLCodec: EntityManager

note right of "XMLCodec":NOTE:Passo os objectos do workbook paar as minhas classes Hibernate e persisto o WorkbookToSimple

XMLCodec->Conection: getConection("JDBC_HSQL..")

XMLCodec->IDataSet: CreateDataSet

IDataSet-->XMLCodec: DataSet

XMLCodec->FlatXMLDataSet: write(DataSet, Stream)

note right of "XMLCodec":NOTE:O m�todo write ir� escrever o xml, tal como ele esta na base de dados. As vers�es ficam ao encargo do hibernate que cria um id unico para cada workbook que existir