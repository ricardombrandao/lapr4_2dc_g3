/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db;

import csheets.core.Address;
import csheets.core.IllegalValueTypeException;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.io.Codec;
import csheets.io.CodecFactory;
import csheets.io.XMLCodec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Luis Mendes
 */
public class XMLCodecTest {
    
    @Test
    public void testSaveXML () throws FormulaCompilationException, IllegalValueTypeException{
           
        int indexPage =1;
        Workbook workB = new Workbook(indexPage);
        Address cellA1 = new Address(0,0);
        Address cellA2 = new Address(0,1);
        
        Address cellB1 = new Address(1,0);
        Address cellB2 = new Address(1,1);
        
   
        workB.getSpreadsheet(indexPage-1).getCell(cellA1).setContent("2");// usa celula A1
        System.out.println("Celula "+cellA1.toString()  + " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellA1).getContent().toString());
        
        workB.getSpreadsheet(indexPage-1).getCell(cellA2).setContent("2");// usa celula A2
        System.out.println("Celula "+cellA2.toString() + " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellA2).getContent().toString());
        
        workB.getSpreadsheet(indexPage-1).getCell(cellB1).setContent("#B2:=sum(A1:A2)"); // formula em B1
        System.out.println("Celua "+cellB1.toString()+" - > "+workB.getSpreadsheet(indexPage-1).getCell(cellB1).getContent());
        
       
        
        
        System.out.println("////////////////////////////////Test Saving Via [ Workbook -> Hibernate -> dbUnit ]//////////////////////////////////////");
        

            String resultSave = "false";
            try {
                File file = new File("unitTest.xml");
                Codec codec = new CodecFactory().getCodec(file);
                XMLCodec xcodec = (XMLCodec)codec;
                FileOutputStream stream;
                stream = new FileOutputStream(file);
                xcodec.write(workB, stream, file);
                
                resultSave = "true";
                System.out.println("Entrou");
                
                
           } catch (Exception ex) {
                System.out.println("Erro");
            }
            
        assertEquals("true",  resultSave); //testa se gravou com secesso

        System.out.println("////////////////////////////////////////////////////////////////////////////////////////////////////");
        
    }
    
    @Test
    public void testLoadXML () throws FormulaCompilationException, IllegalValueTypeException{
           
        testSaveXML();
        System.out.println("////////////////////////////////////////Test load Saved XML //////////////////////////////////////");
        
            
            String resultSave = "false";
            try {
                File file = new File("unitTest.xml");
                Codec codec = new CodecFactory().getCodec(file);
                XMLCodec xcodec = (XMLCodec)codec;
                FileInputStream stream;
                stream = new FileInputStream(file);
                xcodec.read(stream);
                
                resultSave = "true";
                System.out.println("Entrou");
                
                
           } catch (Exception ex) {
                System.out.println("Erro");
            }
            
        assertEquals("true",  resultSave); //testa se leu com secesso

        System.out.println("////////////////////////////////////////////////////////////////////////////////////////////////////");
        
    }
    
     @Test
    public void testWriteXML_LoadXML_VerifyData () throws FormulaCompilationException, IllegalValueTypeException{
          
        int indexPage =1;
        Workbook workB = new Workbook(indexPage);
        Address cellA1 = new Address(0,0);
        Address cellA2 = new Address(0,1);
        
        Address cellB1 = new Address(1,0);
        Address cellB2 = new Address(1,1);
        
   
        workB.getSpreadsheet(indexPage-1).getCell(cellA1).setContent("2");// usa celula A1
        System.out.println("Celula "+cellA1.toString()  + " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellA1).getContent().toString());
        
        workB.getSpreadsheet(indexPage-1).getCell(cellA2).setContent("2");// usa celula A2
        System.out.println("Celula "+cellA2.toString() + " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellA2).getContent().toString());
        
        workB.getSpreadsheet(indexPage-1).getCell(cellB1).setContent("#B2:=sum(A1:A2)"); // formula em B1
        System.out.println("Celua "+cellB1.toString()+" - > "+workB.getSpreadsheet(indexPage-1).getCell(cellB1).getContent());
        
       
        
        
        System.out.println("////////////////////////////////Test Saving Via [ Workbook -> Hibernate -> dbUnit ]//////////////////////////////////////");
        

            String resultSave = "false";
            try {
                File file = new File("unitTest.xml");
                Codec codec = new CodecFactory().getCodec(file);
                XMLCodec xcodec = (XMLCodec)codec;
                FileOutputStream stream;
                stream = new FileOutputStream(file);
                xcodec.write(workB, stream, file);
                
                resultSave = "true";
                System.out.println("Entrou");
                
                
           } catch (Exception ex) {
                System.out.println("Erro");
            }
            
        assertEquals("true",  resultSave); //testa se gravou com secesso

        System.out.println("////////////////////////////////////////////////////////////////////////////////////////////////////");
         System.out.println("////////////////////////////////////////Test load Saved XML //////////////////////////////////////");
        
            
            resultSave = "false";
            try {
                File file = new File("unitTest.xml");
                Codec codec = new CodecFactory().getCodec(file);
                XMLCodec xcodec = (XMLCodec)codec;
                FileInputStream stream;
                stream = new FileInputStream(file);
                xcodec.read(stream);
                
                resultSave = "true";
                System.out.println("Entrou");
                
                
           } catch (Exception ex) {
                System.out.println("Erro");
            }
            
        assertEquals("true",  resultSave); //testa se leu com secesso

        System.out.println("////////////////////////////////////////////////////////////////////////////////////////////////////");
        
        assertEquals(4,  workB.getSpreadsheet(indexPage-1).getCell(cellB2).getValue().toDouble(),0.1); //testa a celula de destino
        assertEquals(4,  workB.getSpreadsheet(indexPage-1).getCell(cellB1).getValue().toDouble(),0.1); //testa a celula onde foi colocada a formula
        assertEquals("#B2:=sum(A1:A2)" , workB.getSpreadsheet(indexPage-1).getCell(cellB1).getContent()); //testa a formula presente na celula
        
    }
    
    
    
}
