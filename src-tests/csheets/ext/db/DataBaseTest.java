/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db;
import csheets.core.Address;
import csheets.core.Cell;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Scare
 */
public class DataBaseTest {
    
    @Test
    public void testCreateTable() throws FormulaCompilationException{
        DataBaseFactory db = new DataBaseFactory("HSQLDB");
        Object obj = db.getObjectDriver();
        DataBaseUtilHSQLDB driverBD = (DataBaseUtilHSQLDB)obj;
        ArrayList<String> colunas = devolveColunasTabela(devolveMatriz());
        
        assertEquals("coluna1",colunas.get(0).toString());
        assertEquals("coluna2",colunas.get(1).toString());
        assertEquals("coluna3",colunas.get(2).toString());
        
    }
    
    
    
    @Test
    public void testCheckTableExistance() throws FormulaCompilationException{
        DataBaseFactory db = new DataBaseFactory("HSQLDB");
        Object obj = db.getObjectDriver();
        DataBaseUtilHSQLDB driverBD = (DataBaseUtilHSQLDB)obj;
        
        try{
            //Tem de mudar o caminho para gravar a base de dados porque 
            //nao conseguimos gravar no C:\ "supostamente" não temos privilegios
        driverBD.ligarBaseDados("C:\\Users\\Scare\\Documents\\DataBase", "SA", "");
        String tabelaInsert = driverBD.criarTabela("Tabela", devolveMatriz(), false, null);
        //Isto está a dar erro porque o metodo insertQuery é privado mas eu quero que assim seja
        //Para numa classe exterior não podemos mexer directamente nas bases de dados sem passar por validações
        driverBD.insertQuery(tabelaInsert);
        boolean b = driverBD.checkTableExistence("Tabela");
        driverBD.desligarBaseDados();
        assertEquals(true,b);
        
        }catch(Exception ex){
            System.out.println("DataBase não quer trabalhar");
            System.out.println("Tem de mudar o caminho para gravar a base de dados porque \n nao conseguimos gravar no C:\\ \"supostamente\" não temos privilegios");
        }
    }
        
    public Cell[][] devolveMatriz() throws FormulaCompilationException{
        Address adress1 = new Address(0,0);
        Address adress2 = new Address(0,1);
        Address adress3 = new Address(0,2);
        Address adress111 = new Address(1,0);
        Address adress112 = new Address(1,1);
        Address adress113 = new Address(1,2);
        Address adress121 = new Address(2,0);
        Address adress122 = new Address(2,1);
        Address adress123 = new Address(2,2);
        Address adress131 = new Address(3,0);
        Address adress132 = new Address(3,1);
        Address adress133 = new Address(3,2);
        Workbook workbook = new Workbook(1);
        workbook.getSpreadsheet(0).getCell(adress1).setContent("coluna1");
        workbook.getSpreadsheet(0).getCell(adress2).setContent("coluna2");
        workbook.getSpreadsheet(0).getCell(adress3).setContent("coluna3");
        workbook.getSpreadsheet(0).getCell(adress111).setContent("1");
        workbook.getSpreadsheet(0).getCell(adress112).setContent("2");
        workbook.getSpreadsheet(0).getCell(adress113).setContent("3");
        workbook.getSpreadsheet(0).getCell(adress121).setContent("11");
        workbook.getSpreadsheet(0).getCell(adress122).setContent("22");
        workbook.getSpreadsheet(0).getCell(adress123).setContent("33");
        workbook.getSpreadsheet(0).getCell(adress131).setContent("111");
        workbook.getSpreadsheet(0).getCell(adress132).setContent("222");
        workbook.getSpreadsheet(0).getCell(adress133).setContent("333");
        Cell celula1;
        Cell celula2;
        Cell celula3;
        Cell celula111;
        Cell celula112;
        Cell celula113;
        Cell celula121;
        Cell celula122;
        Cell celula123;
        Cell celula131;
        Cell celula132;
        Cell celula133;
        celula1 = workbook.getSpreadsheet(0).getCell(adress1);
        celula2 = workbook.getSpreadsheet(0).getCell(adress2);
        celula3 = workbook.getSpreadsheet(0).getCell(adress3);
        celula111 = workbook.getSpreadsheet(0).getCell(adress111);
        celula112 = workbook.getSpreadsheet(0).getCell(adress112);
        celula113 = workbook.getSpreadsheet(0).getCell(adress113);
        celula121 = workbook.getSpreadsheet(0).getCell(adress121);
        celula122 = workbook.getSpreadsheet(0).getCell(adress122);
        celula123 = workbook.getSpreadsheet(0).getCell(adress123);
        celula131 = workbook.getSpreadsheet(0).getCell(adress131);
        celula132 = workbook.getSpreadsheet(0).getCell(adress132);
        celula133 = workbook.getSpreadsheet(0).getCell(adress133);
          
        Cell[][] celulas = { {celula1, celula2, celula3}, {celula111, celula112, celula113}, {celula121, celula122, celula123} , {celula131, celula132, celula133}};
        
        return celulas;
    }



    private ArrayList<String> devolveColunasTabela(Cell[][] cel){
        //Armazena nomes das colunas na arraylist
        ArrayList<String> colunas = new ArrayList<String>();
        int cont=0;
        for (Cell[] row : cel) {
            for (Cell cell : row) {
                cont++;
                if(cell.getValue().toString().equals("")){
                    colunas.add("Column"+cont);}
                else{
                    colunas.add(cell.getValue().toString());}
            }
            break;
        }
        //System.out.println("Numero de colunas: "+colunas.size());
        return colunas;
    }
}
