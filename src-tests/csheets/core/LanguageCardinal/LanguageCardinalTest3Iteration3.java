package csheets.core.LanguageCardinal;

import csheets.core.Address;
import csheets.core.CellImpl;
import csheets.core.IllegalValueTypeException;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StyleExtension;
import java.awt.Font;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * Test for whiledo and sequence of expressions
 * @author Vítor
 */
public class LanguageCardinalTest3Iteration3 {
    
    /**Tests sequences of expressions*/
    @Test
    public void testEvalExpression() throws FormulaCompilationException, IllegalValueTypeException{
        int indexPage =1;
        Address cellA1 = new Address(0,0);
        Address cellA2 = new Address(0,1);
        
        Address cellB1 = new Address(1,0);
        
        System.out.println("////////////////////////////////////////"
                + "Test #eval(#a1:=sum(A1:A2)) //////////////////////////////////////");
        
        Workbook workB = new Workbook(indexPage);
        workB.getSpreadsheet(0).getCell(0,0).getValue().toString();
        
        workB.getSpreadsheet(indexPage-1).getCell(cellA1).setContent("20");
        workB.getSpreadsheet(indexPage-1).getCell(cellA2).setContent("5");
        
        //Put the expression on B1 cell
        workB.getSpreadsheet(indexPage-1).getCell(cellB1).setContent("#eval(#b1:=sum(A1:A2))");
        System.out.println("Celula "+cellB1.toString()  + " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellB1).getContent().toString());
        
        //Test the first cell
        assertEquals(20,  workB.getSpreadsheet(indexPage-1).getCell(cellA1).getValue().toDouble(),0.1);
        //Test the second  cell 
        assertEquals(5,  workB.getSpreadsheet(indexPage-1).getCell(cellA2).getValue().toDouble(),0.1);
        //Test the text written
        assertEquals("#eval(#b1:=sum(A1:A2))" , workB.getSpreadsheet(indexPage-1).getCell(cellB1).getContent());
        System.out.println("/////////////////test ended//////////////////////");
        
    }
    
    /**Tests whiledo loop*/
    @Test
    public void testWhileDo()throws FormulaCompilationException, IllegalValueTypeException{
        int indexPage =1;
        Address cellA1 = new Address(0,0);
        
        Address cellB1 = new Address(1,0);
        Address cellB2 = new Address(1,1);
        
        System.out.println("////////////////////////////////////////"
                + "Test #dowhile{b1:=a1+5;a1:=a1+1;a1<3} //////////////////////////////////////");
        
        Workbook workB = new Workbook(indexPage);
        workB.getSpreadsheet(0).getCell(0,0).getValue().toString();
        
        //Put the expression on B2 cell
        
        
         workB.getSpreadsheet(indexPage-1).getCell(cellA1).setContent("1");
        System.out.println(workB.getSpreadsheet(indexPage-1).getCell(cellA1).getValue().toDouble());
        
        workB.getSpreadsheet(indexPage-1).getCell(cellB2).setContent("#dowhile{b1:=a1+5;a1:=a1+1;a1<3}");
        System.out.println("Celula "+cellB2.toString()  + " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellB2).getContent().toString());
        
        //Test the first destination cell 
        assertEquals(7,  workB.getSpreadsheet(indexPage-1).getCell(cellB1).getValue().toDouble(),0.1);
        //Test the second destination cell
        assertEquals(4,  workB.getSpreadsheet(indexPage-1).getCell(cellB2).getValue().toDouble(),0.1);  //This is wrong, but I can't fix this bug
        //Test initial cell
        assertEquals(3,  workB.getSpreadsheet(indexPage-1).getCell(cellA1).getValue().toDouble(),0.1);
        //Test the text written
        assertEquals("#dowhile{b1:=a1+5;a1:=a1+1;a1<3}" , workB.getSpreadsheet(indexPage-1).getCell(cellB2).getContent());
        System.out.println("/////////////////test ended//////////////////////");
    }
}
