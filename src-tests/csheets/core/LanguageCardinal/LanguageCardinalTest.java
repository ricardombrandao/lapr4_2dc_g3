/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.core.LanguageCardinal;

import csheets.core.Address;
import csheets.core.IllegalValueTypeException;
import csheets.core.Workbook;
import csheets.core.formula.compiler.FormulaCompilationException;
import org.junit.*;
import static org.junit.Assert.*;
/**
 *
 * @author Luis Mendes
 */
public class LanguageCardinalTest {
    @Test
    public void testeAttributionSum () throws FormulaCompilationException, IllegalValueTypeException{
        int indexPage =1;
        Address cellA1 = new Address(0,0);
        Address cellA2 = new Address(0,1);
        
        Address cellB1 = new Address(1,0);
        Address cellB2 = new Address(1,1);
        
        System.out.println("////////////////////////////////////////Teste #B2:=sum(A1:A2) //////////////////////////////////////");
        
        Workbook workB = new Workbook(indexPage);
        
        // atribuir valor de testes as celulas a testar      
        workB.getSpreadsheet(indexPage-1).getCell(cellA1).setContent("2");// usa celula A1
        System.out.println("Celula "+cellA1.toString()  + " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellA1).getContent().toString());
        
        workB.getSpreadsheet(indexPage-1).getCell(cellA2).setContent("2");// usa celula A2
        System.out.println("Celula "+cellA2.toString() + " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellA2).getContent().toString());
        
        workB.getSpreadsheet(indexPage-1).getCell(cellB1).setContent("#B2:=sum(A1:A2)"); // formula em B1
        System.out.println("Celua "+cellB1.toString()+" - > "+workB.getSpreadsheet(indexPage-1).getCell(cellB1).getContent());
        
        assertEquals(4,  workB.getSpreadsheet(indexPage-1).getCell(cellB2).getValue().toDouble(),0.1); //testa a celula de destino
        assertEquals(4,  workB.getSpreadsheet(indexPage-1).getCell(cellB1).getValue().toDouble(),0.1); //testa a celula onde foi colocada a formula
        assertEquals("#B2:=sum(A1:A2)" , workB.getSpreadsheet(indexPage-1).getCell(cellB1).getContent()); //testa a formula presente na celula
        System.out.println("////////////////////////////////////////////////////////////////////////////////////////////////////");
        
    }
    
      @Test
    public void testeAttributionSimple () throws FormulaCompilationException, IllegalValueTypeException{
        int indexPage =1;
        
        Address cellA1 = new Address(0,0);
        
        System.out.println("////////////////////////////////////////Teste #B1:=2 //////////////////////////////////////");
        
        Workbook workB = new Workbook(indexPage);
        
        // atribuir valor de testes as celulas a testar
        workB.getSpreadsheet(indexPage-1).getCell(cellA1).setContent("#B1:=2"); // formula em A1
        
        assertEquals(2,  workB.getSpreadsheet(indexPage-1).getCell(cellA1).getValue().toDouble(),0.1); //testa a celula de destino
        assertEquals("#B1:=2",  workB.getSpreadsheet(indexPage-1).getCell(cellA1).getContent()); //testa a formula presente na celula
        System.out.println("Celua "+cellA1.toString()+" - > "+workB.getSpreadsheet(indexPage-1).getCell(cellA1).getContent());
        System.out.println("////////////////////////////////////////////////////////////////////////////////////////////////////");       
    }
    
    @Test
    public void testeCardinalSum () throws FormulaCompilationException, IllegalValueTypeException{
        int indexPage =1;
        Address cellA1 = new Address(0,0);
        Address cellA2 = new Address(0,1);
        
        Address cellB1 = new Address(1,0);

        System.out.println("////////////////////////////////////////Teste #sum(A1:A2) //////////////////////////////////////");
        
        Workbook workB = new Workbook(indexPage);
        
        // atribuir valor de testes as celulas a testar
        
        workB.getSpreadsheet(indexPage-1).getCell(new Address(0,0)).setContent("2");// usa celula A1
        System.out.println("Celula "+cellA1.toString()  + " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellA1).getContent().toString());
        
        workB.getSpreadsheet(indexPage-1).getCell(new Address(0,1)).setContent("2");// usa celula A2
        System.out.println("Celula "+cellA2+ " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellA2).getContent().toString());

        workB.getSpreadsheet(indexPage-1).getCell(cellB1).setContent("#sum(A1:A2)"); // formula em B1
        System.out.println("Celua "+cellB1.toString()+" - > "+workB.getSpreadsheet(indexPage-1).getCell(cellB1).getContent());
        
        assertEquals(4,  workB.getSpreadsheet(indexPage-1).getCell(cellB1).getValue().toDouble(),0.1); //testa a celula de destino
        assertEquals("#sum(A1:A2)",  workB.getSpreadsheet(indexPage-1).getCell(cellB1).getContent()); //testa a formula presente na celula
        System.out.println("////////////////////////////////////////////////////////////////////////////////////////////////////");       
    }
      
    @Test
    public void testeCardinalAVG () throws FormulaCompilationException, IllegalValueTypeException{
        int indexPage =1;
        Address cellA1 = new Address(0,0);
        Address cellA2 = new Address(0,1);
        
        Address cellB1 = new Address(1,0);

        System.out.println("////////////////////////////////////////Teste #average(A1:A2) //////////////////////////////////////");
        
        Workbook workB = new Workbook(indexPage);
        
        // atribuir valor de testes as celulas a testar
        
        workB.getSpreadsheet(indexPage-1).getCell(new Address(0,0)).setContent("2");// usa celula A1
        System.out.println("Celula "+cellA1.toString()  + " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellA1).getContent().toString());
        
        workB.getSpreadsheet(indexPage-1).getCell(new Address(0,1)).setContent("4");// usa celula A2
        System.out.println("Celula "+cellA2+ " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellA2).getContent().toString());

        workB.getSpreadsheet(indexPage-1).getCell(cellB1).setContent("#average(A1:A2)"); // formula em B1
        System.out.println("Celua "+cellB1.toString()+" - > "+workB.getSpreadsheet(indexPage-1).getCell(cellB1).getContent());
        
        assertEquals(3,  workB.getSpreadsheet(indexPage-1).getCell(cellB1).getValue().toDouble(),0.1); //testa a celula de destino
        assertEquals("#average(A1:A2)",  workB.getSpreadsheet(indexPage-1).getCell(cellB1).getContent()); //testa a formula presente na celula
        
        System.out.println("////////////////////////////////////////////////////////////////////////////////////////////////////");
    }
      
    
    @Test
    public void testeCardinalSubtractor() throws FormulaCompilationException, IllegalValueTypeException{
        int indexPage =1;
        Address cellA1 = new Address(0,0);
        Address cellA2 = new Address(0,1);
        
        Address cellB1 = new Address(1,0);
        Address cellB2 = new Address(1,1);
        
        System.out.println("////////////////////////////////////////Teste #B2:=A1-A2 //////////////////////////////////////");
        
        Workbook workB = new Workbook(indexPage);
        
        // atribuir valor de testes as celulas a testar      
        workB.getSpreadsheet(indexPage-1).getCell(cellA1).setContent("2");// usa celula A1
        System.out.println("Celula "+cellA1.toString()  + " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellA1).getContent().toString());
        
        workB.getSpreadsheet(indexPage-1).getCell(cellA2).setContent("2");// usa celula A2
        System.out.println("Celula "+cellA2.toString() + " -> " + workB.getSpreadsheet(indexPage-1).getCell(cellA2).getContent().toString());
        
        workB.getSpreadsheet(indexPage-1).getCell(cellB1).setContent("#B2:=A1-A2"); // formula em B1
        System.out.println("Celua "+cellB1.toString()+" - > "+workB.getSpreadsheet(indexPage-1).getCell(cellB1).getContent());
        
        assertEquals(0,  workB.getSpreadsheet(indexPage-1).getCell(cellB2).getValue().toDouble(),0.1); //testa a celula de destino
        assertEquals(0,  workB.getSpreadsheet(indexPage-1).getCell(cellB1).getValue().toDouble(),0.1); //testa a celula onde foi colocada a formula
        assertEquals("#B2:=A1-A2" , workB.getSpreadsheet(indexPage-1).getCell(cellB1).getContent()); //testa a formula presente na celula
        System.out.println("////////////////////////////////////////////////////////////////////////////////////////////////////");
    }
    
    
}
