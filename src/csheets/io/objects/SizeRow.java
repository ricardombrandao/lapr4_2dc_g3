/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.io.objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Luis Mendes
 */
@Entity
@Table(name="SizeLinha")
public class SizeRow {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
       
     private int linha;
     private int number;

    public SizeRow() {
    }
     
     
   public SizeRow(int numb, int size){
       linha=size;
       number=numb;
    }

    /**
     * @return the row
     */
    public int getRow() {
        return linha;
    }

    /**
     * @param row the row to set
     */
    public void setRow(int row) {
        this.linha = row;
    }

    /**
     * @return the line
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param line the line to set
     */
    public void setNumber(int number) {
        this.number = number;
    }
    
}
