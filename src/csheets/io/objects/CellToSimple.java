/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.io.objects;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Luis Mendes
 */
@Entity
@Table(name="Cell")
public class CellToSimple implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    private String value;
    private String content;
    private int address1;
    private int address2;
    private int border;
    private String color;
    private String fontText;
    private int sizeText; 
    private int styleText;
    private int backColor;
    
    private int borderLeft;
    private int borderRight;
    private int borderBottom;
    private int borderTop;
    
    private int borderColor;
    private int foreColor;
    
    

    public CellToSimple() {
    }
    
    public CellToSimple(int ad1, int ad2){
        address1=ad1;
        address2=ad2;
        content=null;
        border=0;
        color=null;
        value=null;
        fontText=null;
        sizeText=0; 
        styleText=0;
        backColor=0;
        borderTop=0;
        borderRight=0;
        borderLeft=0;
        borderBottom=0;
        borderColor=0;
        foreColor=0;

    }
   
    
    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String formula) {
        this.content = formula;
    }

    /**
     * @return the address1
     */
    public int getAddress1() {
        return address1;
    }

    /**
     * @param address1 the address1 to set
     */
    public void setAddress1(int address1) {
        this.address1 = address1;
    }

    /**
     * @return the address2
     */
    public int getAddress2() {
        return address2;
    }

    /**
     * @param address2 the address2 to set
     */
    public void setAddress2(int address2) {
        this.address2 = address2;
    }

    /**
     * @return the border
     */
    public int getBorder() {
        return border;
    }

    /**
     * @param border the border to set
     */
    public void setBorder(int border) {
        this.border = border;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the fontText
     */
    public String getFontText() {
        return fontText;
    }

    /**
     * @param fontText the fontText to set
     */
    public void setFontText(String fontText) {
        this.fontText = fontText;
    }

    /**
     * @return the sizeText
     */
    public int getSizeText() {
        return sizeText;
    }

    /**
     * @param sizeText the sizeText to set
     */
    public void setSizeText(int sizeText) {
        this.sizeText = sizeText;
    }

    /**
     * @return the styleText
     */
    public int getStyleText() {
        return styleText;
    }

    /**
     * @param styleText the styleText to set
     */
    public void setStyleText(int styleText) {
        this.styleText = styleText;
    }

    /**
     * @return the backColor
     */
    public int getBackColor() {
        return backColor;
    }

    /**
     * @param backColor the backColor to set
     */
    public void setBackColor(int backColor) {
        this.backColor = backColor;
    }

    /**
     * @return the borderLeft
     */
    public int getBorderLeft() {
        return borderLeft;
    }

    /**
     * @param borderLeft the borderLeft to set
     */
    public void setBorderLeft(int borderLeft) {
        this.borderLeft = borderLeft;
    }

    /**
     * @return the borderRight
     */
    public int getBorderRight() {
        return borderRight;
    }

    /**
     * @param borderRight the borderRight to set
     */
    public void setBorderRight(int borderRight) {
        this.borderRight = borderRight;
    }

    /**
     * @return the borderBottom
     */
    public int getBorderBottom() {
        return borderBottom;
    }

    /**
     * @param borderBottom the borderBottom to set
     */
    public void setBorderBottom(int borderBottom) {
        this.borderBottom = borderBottom;
    }

    /**
     * @return the borderTop
     */
    public int getBorderTop() {
        return borderTop;
    }

    /**
     * @param borderTop the borderTop to set
     */
    public void setBorderTop(int borderTop) {
        this.borderTop = borderTop;
    }

    /**
     * @return the borderColor
     */
    public int getBorderColor() {
        return borderColor;
    }

    /**
     * @param borderColor the borderColor to set
     */
    public void setBorderColor(int borderColor) {
        this.borderColor = borderColor;
    }

    /**
     * @return the foreColor
     */
    public int getForeColor() {
        return foreColor;
    }

    /**
     * @param foreColor the foreColor to set
     */
    public void setForeColor(int foreColor) {
        this.foreColor = foreColor;
    }
    
    
}
