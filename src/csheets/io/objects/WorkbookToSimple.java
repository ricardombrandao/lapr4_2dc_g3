/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.io.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Luis Mendes
 */
@Entity
@Table(name="Workbook")
public class WorkbookToSimple implements Serializable {
    @Id
    @GeneratedValue
    private int id;

    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date data = new Date();
    
    @OneToMany(cascade = CascadeType.ALL)
    private List<SpreadsheetToSimple> listSpreadsheet= new ArrayList<SpreadsheetToSimple>();
    
    
    private int number;
    private String name;
    private long hour;

    public WorkbookToSimple() {
    }
    
    public WorkbookToSimple(String name, int number){
        this.name=name;
        this.number=number;
        hour = data.getTime();

        
    }

    /**
     * @return the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the listSpreadsheet
     */
    public List<SpreadsheetToSimple> getListSpreadsheet() {
        return listSpreadsheet;
    }

    /**
     * @param listSpreadsheet the listSpreadsheet to set
     */
    public void setListSpreadsheet(List<SpreadsheetToSimple> listSpreadsheet) {
        this.listSpreadsheet = listSpreadsheet;
    }
    
    public void addListSpreadsheet(SpreadsheetToSimple listSpreadsheet) {
        this.listSpreadsheet.add(listSpreadsheet);
    }
    
}
