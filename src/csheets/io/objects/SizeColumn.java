/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.io.objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Luis Mendes
 */
@Entity
@Table(name="SizeColuna")
public class SizeColumn {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
       
     private int coluna;
     private int number;

    public SizeColumn() {
    }
     
     
   public SizeColumn(int numb, int size){
       coluna=size;
       number=numb;
    }

    /**
     * @return the column
     */
    public int getColumn() {
        return coluna;
    }

    /**
     * @param column the column to set
     */
    public void setColumn(int col) {
        this.coluna = col;
    }

    /**
     * @return the line
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param line the line to set
     */
    public void setNumber(int number) {
        this.number = number;
    }
    
}
