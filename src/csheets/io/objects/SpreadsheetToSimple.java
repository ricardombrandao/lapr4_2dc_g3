/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.io.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Luis Mendes
 */
@Entity
@Table(name="Spreadsheet")
public class SpreadsheetToSimple implements Serializable {
        
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    private int number;
    private String title;

    @OneToMany(cascade = CascadeType.ALL)
    private List<CellToSimple> listCell;

    @OneToMany(cascade = CascadeType.ALL)
    private List<SizeRow> listRowSize;

    @OneToMany(cascade = CascadeType.ALL)
    private List<SizeColumn> listColumnSize;

    
    public SpreadsheetToSimple() {
    }
    
    public SpreadsheetToSimple(String title, int number ){
        this.title=title;
        this.number= number;
        listCell = new ArrayList<CellToSimple>();
        listRowSize = new ArrayList<SizeRow>();
        listColumnSize = new ArrayList<SizeColumn>();
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(int number) {
        this.number = number;
    }
    
    public void addList(CellToSimple cell){
        getListCell().add(cell);
    
    }

    /**
     * @return the listCell
     */
    public List<CellToSimple> getListCell() {
        return listCell;
    }

    /**
     * @param listCell the listCell to set
     */
    public void setListCell(List<CellToSimple> listCell) {
        this.listCell = listCell;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the listRowSize
     */
    public List<SizeRow> getListRowSize() {
        return listRowSize;
    }

    /**
     * @param listRowSize the listRowSize to set
     */
    public void setListRowSize(List<SizeRow> listSize) {
        this.listRowSize = listSize;
    }
 
    public void addRowSize(SizeRow a) {
        listRowSize.add(a);
    }

    /**
     * @return the listColumnSize
     */
    public List<SizeColumn> getListColumnSize() {
        return listColumnSize;
    }

    /**
     * @param listColumnSize the listColumnSize to set
     */
    public void setListColumnSize(List<SizeColumn> listColumnSize) {
        this.listColumnSize = listColumnSize;
    }
    public void addColumnSize(SizeColumn a) {
        listColumnSize.add(a);
    }
    
    
    
}
