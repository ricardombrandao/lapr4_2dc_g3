package csheets.io;

import csheets.core.Address;
import csheets.core.Spreadsheet;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import csheets.core.Workbook;
import csheets.ext.db.PersistenceInit;
import csheets.ext.db.ui.PainelMessage;
import csheets.ext.persistence.ui.PersistencePanel;
import csheets.ext.style.StylableCell;
import csheets.ext.style.StylableSpreadsheet;
import csheets.ext.style.StyleExtension;
import csheets.io.objects.CellToSimple;
import csheets.io.objects.SizeColumn;
import csheets.io.objects.SizeRow;
import csheets.io.objects.SpreadsheetToSimple;
import csheets.io.objects.WorkbookToSimple;
import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;


/**
 * Classe de codec xml, responsável por escrever e ler em formato xml todo o
 * workbook. Esta implementa o xmlCodeckInterface que por sua vez implementa
 * codec. Foi necessária esta alteração pois na escrita para suporte de varias
 * versões foi necessário ter acesso ao nome do ficheiro, a interface codec não
 * o permitia, então para isso foi criado esta interface que apenas tem o método
 * write que além do workbook e stream de output tem o nome do ficheiro. Para
 * isto ser implementado foi necessário ir a classe main principal
 * CleanSheets.java e colocar um if na calsse Save AS para quando o tipo
 * escrita xml ele enviar mais este parâmetro. Foi efectuado uma longa pesquisa
 * para tentar colocar o dbunit a escrever um xml com uma estortura hierárquica
 * mas não foi obtido qualquer resultado. As únicas formas de imprimir são por
 * tabelas todas ao mesmo nível. A cada save no mesmo documento o hibernate /
 * sql atribui um id único ao novo workbook, e assim guarda este novo workbook
 * com todas as suas spreadsheets e células. Para carregar para o programa (um
 * novo workbook) é feito um select que obtém todos os elementos com o id
 * workbook mais alto (ultimo a ser gravado) e assim fica na base de dados
 * hibernate todas as versões mas apenas a ultima é carregada para a memória.
 * Para utilizar o hibernate sem editar as classes base foram criadas 3 classes
 * no package cheets.io.objects apenas pagar persistir os dados do Workbook para
 * o hibernate.
 */
public class XMLCodec implements XMLCodecInterface {

    protected List<Object> listaResults;
    protected List<Integer> listId;
    private OutputStream output;
    File filename;

    /**
     * Creates a new XML codec.
     */
    public XMLCodec() {
        listaResults = new ArrayList<Object>();
        listId = new ArrayList<Integer>();
    }

    public List<Object> getListaResults() {
        return listaResults;
    }

    public List<Integer> getListId() {
        return listId;
    }

    public Workbook read(InputStream stream) throws IOException {
        System.out.println("Reading");
        ////////////////////////////////////////////////////////////////////////////////////
        // apaga a base de dados em memoruia se ela exitir e importa
        Connection jdbcConnection = null;
        try {
            Class.forName("org.hsqldb.jdbcDriver");
            jdbcConnection = DriverManager.getConnection("jdbc:hsqldb:file:temporary", "sa", "");
            IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);

            IDataSet fullDataSet = new FlatXmlDataSet(stream);
            DatabaseOperation.CLEAN_INSERT.execute(connection, fullDataSet);

            /////////////////////////////////////////////////////////////////////////////////
            // cria os objectos qur foram guardados pelo hibernate
            EntityManager manager = PersistenceInit.init();
            manager.getTransaction().begin();

            //select dos dados pelo id do workbook mais alto (ultimo a ser gravado)
            Query data = manager.createQuery("select workbook from WorkbookToSimple workbook where workbook.id = (select MAX(table2.id) from WorkbookToSimple table2)");

            listaResults = data.getResultList();

            Statement st = null;
            ResultSet rs = null;
            st = jdbcConnection.createStatement();         // statement objects can be reused with
            rs = st.executeQuery("Select id from Workbook");

            while (rs.next()) {

                int valcount;
                valcount = Integer.parseInt(rs.getString(1));
                listId.add(valcount);
            }
            jdbcConnection.close();
        } catch (Exception ex) {
            System.out.println("Erro read!");
            try {
                jdbcConnection.close();
            } catch (SQLException ex1) {
            }
        }

        //inicializar o workbook de retorno
        Workbook workbook = null;

        if (listaResults.isEmpty() == false) {

            WorkbookToSimple wbs = (WorkbookToSimple) listaResults.get(0);

            Spreadsheet spread;
            workbook = new Workbook();
            Address address = null;

            String value = "";

            try {
                for (int k = 0; k < wbs.getListSpreadsheet().size(); k++) { //numero de folhas
                    //informação das spreadsheet
                    List<CellToSimple> cell = wbs.getListSpreadsheet().get(k).getListCell();
                    workbook.addSpreadsheet();
                    spread = workbook.getSpreadsheet(k);
                    spread.setTitle(wbs.getListSpreadsheet().get(k).getTitle()); //definir titulo da folha




                    /////////////// espaçamentos de colunas e linhas ////////////////////

                    for (int i = 0; i < wbs.getListSpreadsheet().get(k).getListRowSize().size(); i++) {
                        StylableSpreadsheet stylableSheet = (StylableSpreadsheet) spread.getExtension(StyleExtension.NAME);
                        SizeRow size = wbs.getListSpreadsheet().get(k).getListRowSize().get(i);
                        stylableSheet.setRowHeight(size.getNumber(), size.getRow());

                    }
                    for (int i = 0; i < wbs.getListSpreadsheet().get(k).getListColumnSize().size(); i++) {
                        StylableSpreadsheet stylableSheet = (StylableSpreadsheet) spread.getExtension(StyleExtension.NAME);
                        SizeColumn size = wbs.getListSpreadsheet().get(k).getListColumnSize().get(i);
                        stylableSheet.setColumnWidth(size.getNumber(), size.getColumn());

                    }

                    /////////////////////////////////////////////////////////////////////
                    for (int j = 0; j < cell.size(); j++) {
                        //informação das celuas
                        address = new Address(cell.get(j).getAddress1(), cell.get(j).getAddress2());

                        spread.getCell(address).setContent(cell.get(j).getContent());

                        //////////////////////////// estilos /////////////////////////////////////////////
                        StylableCell c = (StylableCell) workbook.getSpreadsheet(k).getCell(address).getExtension(StyleExtension.NAME); //cast para estilos
                        cell.get(j).getFontText();
                        cell.get(j).getSizeText();
                        cell.get(j).getSizeText();
                        c.setFont(new Font(cell.get(j).getFontText(), cell.get(j).getSizeText(), cell.get(j).getSizeText()));
                        c.setBackgroundColor(new Color(cell.get(j).getBackColor()));
                        c.setForegroundColor(new Color(cell.get(j).getForeColor()));

                        if (cell.get(j).getBorderColor() != 0) {
                            c.setBorder(new MatteBorder(cell.get(j).getBorderTop(), cell.get(j).getBorderLeft(), cell.get(j).getBorderBottom(), cell.get(j).getBorderRight(), new Color(cell.get(j).getBorderColor())));
                        } else {
                            c.setBorder(new EmptyBorder(cell.get(j).getBorderTop(), cell.get(j).getBorderLeft(), cell.get(j).getBorderBottom(), cell.get(j).getBorderRight()));
                        }

                    }

                }


            } catch (Exception e) {
            }

        } else {
            PainelMessage nome = new PainelMessage();
            nome.PainelOk("No results found in your xml input");
            System.out.println("No resultSet");
        }

        PersistencePanel teste = PersistencePanel.getInstance();
        int size = listId.size();
        String[] values = new String[size];
        for (int i = 0; i < size; i++) {
            values[i] = listId.get(i).toString();
        }

        teste.instanciaSideBar(values);

        return workbook;
    }
    
    @Override
    public void write(Workbook workbook, OutputStream stream, File filename) throws IOException {
        System.out.println("Writing!");
        // Wraps stream
        // PrintWriter writer = new PrintWriter(new BufferedWriter(
        //       new OutputStreamWriter(stream)));

        EntityManager manager = PersistenceInit.init();
        manager.getTransaction().begin();

        //Address toBD;
        int Address1;
        int Address2;
        String Value;


        try {
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            /////////////////////// verifica se existe o nome do ficheiro na base de dados para criar versões ou apagar o conteudo da base de dados
            Connection connection = null;
            connection = DriverManager.getConnection("jdbc:hsqldb:file:temporary", "sa", "");
            Statement st = null;
            ResultSet rs = null;
            st = connection.createStatement();         // statement objects can be reused with
            rs = st.executeQuery("Select count(name) from Workbook where name ='" + filename.getName() + "'");    // run the query

            rs.next();
            int valcount;
            valcount = Integer.parseInt(rs.getString(1));

            connection.close();
            if (valcount == 0) { // se for um novo ficheiro em que não existe registo na base de dados temporaria (hibernate), apaga os dados existentes.
                System.out.println("####################### Deleat all history data tables ###############################");
                Class.forName("org.hsqldb.jdbcDriver");
                Connection jdbcConnection = DriverManager.getConnection("jdbc:hsqldb:file:temporary", "sa", "");
                IDatabaseConnection connection2 = new DatabaseConnection(jdbcConnection);
                IDataSet fullDataSet2 = connection2.createDataSet();

                DatabaseOperation.DELETE_ALL.execute(connection2, fullDataSet2);
                jdbcConnection.close();
            }
            connection.close();
        } catch (Exception e) {
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // create new objects with hibernate tag class, for automatic right in data baseHibernate
        WorkbookToSimple wb = new WorkbookToSimple(filename.getName(), 0);

        for (int k = 0; k < workbook.getSpreadsheetCount(); k++) {
            SpreadsheetToSimple sp = new SpreadsheetToSimple(workbook.getSpreadsheet(k).getTitle(), k);
            wb.addListSpreadsheet(sp);
            for (int j = 0; j <= workbook.getSpreadsheet(k).getColumnCount(); j++) { //colunas
                sp.setTitle(workbook.getSpreadsheet(k).getTitle());
                //largura e altura das colunas e linhas
                StylableSpreadsheet stylableSheet = (StylableSpreadsheet) workbook.getSpreadsheet(k).getExtension(StyleExtension.NAME);
                //stylableSheet.getColumnWidth(j);

                //column sizes
                int size = stylableSheet.getColumnWidth(j);
                SizeColumn col = new SizeColumn(j, size);
                sp.addColumnSize(col);
                System.out.println("Coluna: " + j);
                System.out.println("Tamanho: " + size);


                for (int i = 0; i <= workbook.getSpreadsheet(k).getRowCount(); i++) { //linhas

                    //row sizes
                    int size2 = stylableSheet.getRowHeight(i);
                    SizeRow rows = new SizeRow(i, size2);
                    sp.addRowSize(rows);


                    //if (workbook.getSpreadsheet(0).getCell(j,i).getValue().toString() != ""){
                    Address1 = (new Address(j, i)).getColumn();
                    Address2 = (new Address(j, i)).getRow();
                    Value = workbook.getSpreadsheet(k).getCell(j, i).getValue().toString();
                    CellToSimple cell = new CellToSimple(Address1, Address2); // adress da celula
                    /////////////////////////////////// estilos do texto e celula  //////////////////////////////////////////
                    StylableCell c = (StylableCell) workbook.getSpreadsheet(k).getCell(j, i).getExtension(StyleExtension.NAME); //cast para estilos
                    cell.setFontText(c.getFont().getName());
                    cell.setSizeText(c.getFont().getSize());
                    cell.setStyleText(c.getFont().getStyle());
                    cell.setBackColor(c.getBackgroundColor().getRGB());
                    cell.setForeColor(c.getForegroundColor().getRGB());
                    cell.setValue(Value); // valor na celula

                    cell.setContent(workbook.getSpreadsheet(k).getCell(j, i).getContent()); //ex. =sum(1+1)


                    if (c.getBorder() instanceof MatteBorder) {
                        //Tem cor
                        cell.setBorderColor(((MatteBorder) c.getBorder()).getMatteColor().getRGB());
                        cell.setBorderLeft(((EmptyBorder) c.getBorder()).getBorderInsets().left);
                        cell.setBorderRight(((EmptyBorder) c.getBorder()).getBorderInsets().right);
                        cell.setBorderTop(((EmptyBorder) c.getBorder()).getBorderInsets().top);
                        cell.setBorderBottom(((EmptyBorder) c.getBorder()).getBorderInsets().bottom);

                    } else {
                        cell.setBorderLeft(((EmptyBorder) c.getBorder()).getBorderInsets().left);
                        cell.setBorderRight(((EmptyBorder) c.getBorder()).getBorderInsets().right);
                        cell.setBorderTop(((EmptyBorder) c.getBorder()).getBorderInsets().top);
                        cell.setBorderBottom(((EmptyBorder) c.getBorder()).getBorderInsets().bottom);
                    }

                    sp.addList(cell);
                    //}
                }
            }
        }
        // use hibernate object for persist data in DB    
        manager.persist(wb);
        manager.getTransaction().commit();
        manager.close();



        ///////////////////////////////////////////////////////////////////////
        // end BD creation ////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////
        // creating xml file via dbunit
        try {
            Class.forName("org.hsqldb.jdbcDriver");
            Connection jdbcConnection = DriverManager.getConnection("jdbc:hsqldb:file:temporary", "sa", "");
            IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);

            IDataSet fullDataSet = connection.createDataSet();

            FlatXmlDataSet.write(fullDataSet, stream);
            //DatabaseOperation.DELETE_ALL.execute(connection, fullDataSet);
            jdbcConnection.close();
        } catch (Exception e) {
        }

        //writer.close();
        stream.close();
        System.out.println("Hibernate data base fully saved in xml via dbunit");

    }

    @Override
    public void write(Workbook workbook, OutputStream stream) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * A method that loads a version of the workbook
     * @param id
     * @param workbook 
     */
    public void getVersion(String id, Workbook workbook) {
        Connection jdbcConnection = null;
        try {
            Class.forName("org.hsqldb.jdbcDriver");
            jdbcConnection = DriverManager.getConnection("jdbc:hsqldb:file:temporary", "sa", "");
            EntityManager manager = PersistenceInit.init();
            manager.getTransaction().begin();

            //select dos dados pelo id do workbook mais alto (ultimo a ser gravado)
            Query data = manager.createQuery("select workbook from WorkbookToSimple workbook where workbook.id =" + id);

            listaResults = data.getResultList();

            jdbcConnection.close();
        } catch (Exception ex) {
            System.out.println("Error select");
        }
        System.out.println(workbook.getSpreadsheetCount());

        try {

            while (true) {
                workbook.removeSpreadsheet(workbook.getSpreadsheet(0));
            }
        } catch (Exception er) {
            //Entra sempre aqui porque eu sou fixe
        }
        //for (int m = 0; m<workbook.getSpreadsheetCount();++m)
        //  workbook.removeSpreadsheet(workbook.getSpreadsheet(m));

        //inicializar o workbook de retorno
        //Workbook workbook = null;

        if (!listaResults.isEmpty()) {

            WorkbookToSimple wbs = (WorkbookToSimple) listaResults.get(0);

            Spreadsheet spread;
            //workbook = new Workbook();
            Address address = null;
            String value = "";

            try {
                //JOptionPane.showMessageDialog(null, wbs.getListSpreadsheet().size());
                for (int k = 0; k < wbs.getListSpreadsheet().size(); k++) { //numero de folhas
                    //informação das spreadsheet
                    List<CellToSimple> cell = wbs.getListSpreadsheet().get(k).getListCell();
                    workbook.addSpreadsheet();
                    spread = workbook.getSpreadsheet(k);
                    //JOptionPane.showMessageDialog(null, wbs.getListSpreadsheet().get(k).getTitle().toString());

                    spread.setTitle(wbs.getListSpreadsheet().get(k).getTitle()); //definir titulo da folha


                    /////////////// espaçamentos de colunas e linhas ////////////////////

                    for (int i = 0; i < wbs.getListSpreadsheet().get(k).getListRowSize().size(); i++) {
                        StylableSpreadsheet stylableSheet = (StylableSpreadsheet) spread.getExtension(StyleExtension.NAME);
                        SizeRow size = wbs.getListSpreadsheet().get(k).getListRowSize().get(i);
                        stylableSheet.setRowHeight(size.getNumber(), size.getRow());

                    }
                    for (int i = 0; i < wbs.getListSpreadsheet().get(k).getListColumnSize().size(); i++) {
                        StylableSpreadsheet stylableSheet = (StylableSpreadsheet) spread.getExtension(StyleExtension.NAME);
                        SizeColumn size = wbs.getListSpreadsheet().get(k).getListColumnSize().get(i);
                        stylableSheet.setColumnWidth(size.getNumber(), size.getColumn());

                    }

                    /////////////////////////////////////////////////////////////////////
                    for (int j = 0; j < cell.size(); j++) {
                        //informação das celuas
                        address = new Address(cell.get(j).getAddress1(), cell.get(j).getAddress2());

                        spread.getCell(address).setContent(cell.get(j).getContent());

                        //////////////////////////// estilos /////////////////////////////////////////////
                        StylableCell c = (StylableCell) workbook.getSpreadsheet(k).getCell(address).getExtension(StyleExtension.NAME); //cast para estilos
                        cell.get(j).getFontText();
                        cell.get(j).getSizeText();
                        cell.get(j).getSizeText();
                        c.setFont(new Font(cell.get(j).getFontText(), cell.get(j).getSizeText(), cell.get(j).getSizeText()));
                        c.setBackgroundColor(new Color(cell.get(j).getBackColor()));
                        c.setForegroundColor(new Color(cell.get(j).getForeColor()));

                        if (cell.get(j).getBorderColor() != 0) {
                            c.setBorder(new MatteBorder(cell.get(j).getBorderTop(), cell.get(j).getBorderLeft(), cell.get(j).getBorderBottom(), cell.get(j).getBorderRight(), new Color(cell.get(j).getBorderColor())));
                        } else {
                            c.setBorder(new EmptyBorder(cell.get(j).getBorderTop(), cell.get(j).getBorderLeft(), cell.get(j).getBorderBottom(), cell.get(j).getBorderRight()));
                        }

                    }

                }


            } catch (Exception e) {
            }

        } else {
            PainelMessage nome = new PainelMessage();
            nome.PainelOk("No results found in your xml input");
            System.out.println("No resultSet");
        }
    }

    /**
     * A method that deletes a version from workbook
     * @param id 
     */
    public void deleteVersion(String id) {
        Connection jdbcConnection = null;

        try {
            Class.forName("org.hsqldb.jdbcDriver");
            jdbcConnection = DriverManager.getConnection("jdbc:hsqldb:file:temporary", "sa", "");

            Statement st = null;
            String sql ="delete from workbook_spreadsheet where workbook_id =" + id;
            st = jdbcConnection.createStatement();
            st.executeQuery(sql);
            
            String sql2 = "delete from workbook where id =" + id;
            
            st = jdbcConnection.createStatement();
            st.executeQuery(sql2);
            System.out.println("Deleting...");

            jdbcConnection.close();
            System.out.println("Version deleted");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error delete");
        }
    }
}
