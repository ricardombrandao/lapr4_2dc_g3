/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.io;

import csheets.core.Workbook;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author Luis Mendes
 */
public interface XMLCodecInterface extends Codec{
   
    public void write(Workbook workbook, OutputStream stream, File filename) throws IOException;

    
}
