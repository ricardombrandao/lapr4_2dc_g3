
header {package csheets.core.formula.compiler;}

class FormulaParserCardinal extends Parser;
options {
        k=3;
	buildAST = true;
	defaultErrorHandler = false;
}

/*
 * The start rule for formula expressions.
 * Now suports the all old function placed in lang package, plus attribution of values to referenced cellules
 */
/* content
	: ( expression | literal ) EOF
	; */

expression
	: CAR! (expression_simp | (LBRA! expression_comp RBRA!) | whiledo | dowhile | eval) EOF! 
	;

eval
        : EVAL^ (CAR!|EQ!) (expression_simp | (LBRA! expression_comp RBRA!) | whiledo | dowhile) RPAR!
        ;

expression_comp
        : expression_simp (SEMI^ expression_simp)*
        ;

whiledo
        : WHILEDO^ comparison (SEMI! expression_simp)+ RBRA!
        ;

dowhile
        : DOWHILE^ (attribution SEMI!)+ comparison  RBRA!
        ;

expression_simp
        : (attribution|comparison)
        ;

attribution
	: CELL_REF ATT^ comparison
	;

comparison
	: concatenation
		( ( EQ^ | NEQ^ | GT^ | LT^ | LTEQ^ | GTEQ^ ) concatenation )?
	;

concatenation
	: arithmetic_lowest
		( AMP^ arithmetic_lowest )*
	;

arithmetic_lowest
	:	arithmetic_low
		( ( PLUS^ | MINUS^ ) arithmetic_low )*
	;

arithmetic_low
	:	arithmetic_medium
		( ( MULTI^ | DIV^ ) arithmetic_medium )*
	;

arithmetic_medium
	:	arithmetic_high
		( POWER^ arithmetic_high )?
	;

arithmetic_high
	:	arithmetic_highest ( PERCENT^ )?
	;

arithmetic_highest
	:	( MINUS^ )? atom
	;

atom
	:	function_call
	|	reference
	|	literal
	|	LPAR! comparison RPAR!
	;

function_call
	:	FUNCTION^ 
		( comparison ( SEMI! comparison )* )?
		RPAR!
	;

reference
	:	CELL_REF
		( ( COLON^ ) CELL_REF )?
	|	NAME
	;

literal
	:	NUMBER
	|	STRING
	;

{import csheets.core.formula.lang.Language;

/**
 * A lexer that splits a string into a list of lexical tokens.
 * @author Einar Pehrson
 */
@SuppressWarnings("all")}
class FormulaLexerCardinal extends Lexer;

options {
	k = 4;
	caseSensitive = false;
	caseSensitiveLiterals = false;
}

/* Function calls, named ranges and cell references */
protected LETTER: ('a'..'z') ;

ALPHABETICAL
	:	( ( LETTER )+ LPAR ) => ( LETTER )+ LPAR! {
			try {
				Language.getInstance().getFunction(#getText());
                                if((!(#getText() == "whiledo")) && (!(#getText() == "dowhile")) && (!(#getText() == "eval")))
                                    $setType(FUNCTION);
			} catch (Exception ex) {
				throw new RecognitionException(ex.toString());
			}
		}
	|	( ( "whiledo" ) LBRA ) => ( "whiledo" ) LBRA! {
			try {
				Language.getInstance().getFunction(#getText());
				$setType(WHILEDO);
			} catch (Exception ex) {
				throw new RecognitionException(ex.toString());
			}
		}
        |	( ( "dowhile" ) LBRA ) => ( "dowhile" ) LBRA! {
			try {
				Language.getInstance().getFunction(#getText());
				$setType(DOWHILE);
			} catch (Exception ex) {
				throw new RecognitionException(ex.toString());
			}
		}
        |	( ( "eval" ) LPAR ) => ( "eval" ) LPAR! {
			try {
				Language.getInstance().getFunction(#getText());
				$setType(EVAL);
			} catch (Exception ex) {
				throw new RecognitionException(ex.toString());
			}
		}
	|       ( ABS )? LETTER ( LETTER )?
		( ABS )? ( DIGIT )+ {
			$setType(CELL_REF);
		}
	;

/* String literals, i.e. anything inside the delimiters */
STRING
	:	QUOT!
		(options {greedy=false;}:.)*
		QUOT!
	;
protected QUOT: '"';

/* Numeric literals */
NUMBER: ( DIGIT )+ ( COMMA ( DIGIT )+ )? ;
protected DIGIT : '0'..'9' ;

/* Comparison operators */
CAR		: "#" ;
EQ		: "=" ;
ATT		: ":=" ;
NEQ		: "<>" ;
LTEQ	: "<=" ;
GTEQ	: ">=" ;
GT		: '>' ;
LT		: '<' ;

/* Text operators */
AMP		: '&' ;

/* Arithmetic operators */
PLUS	: '+' ;
MINUS	: '-' ;
MULTI	: '*' ;
DIV		: '/' ;
POWER	: '^' ;
PERCENT : '%' ;

/* Reference operators */
protected ABS : '$' ;
protected EXCL:  '!'  ;
COLON	: ':' ;

/* Miscellaneous operators */
COMMA	: ',' ;
SEMI	: ';' ;
LPAR	: '(' ;
RPAR	: ')' ;
LBRA    : '{' ;
RBRA    : '}' ;


/* White-space (ignored) */
WS: ( ' '
	| '\r' '\n'
	| '\n'
	| '\t'
	)
	{$setType(Token.SKIP);}
	;


