package csheets.core.formula.compiler;

import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import antlr.ANTLRException;
import antlr.collections.AST;
import csheets.core.Cell;
import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.BinaryOperation;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionCall;
import csheets.core.formula.Literal;
import csheets.core.formula.Reference;
import csheets.core.formula.UnaryOperation;
import csheets.core.formula.lang.CellReference;
import csheets.core.formula.lang.Language;
import csheets.core.formula.lang.RangeReference;
import csheets.core.formula.lang.ReferenceOperation;
import csheets.core.formula.lang.UnknownElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Baseado na class "ExcelExpressionCompiler", esta classe é o que permite
 * reconhecer o cracter # como o inicio da expressão. Para isto ser possivel foi
 * adicionado um novo cacter a classe formulaCompilerCardinal.g para poder
 * aceitar o cacater # e := para atribuição (para isto foi tambem alterada a
 * propria linguagem). Para esta class ser carregada visto que é nova, teve se
 * ser adicionado o seu nome ao ficheiro compilers.props, que é o ficheiro
 * carregado pela fabrica já existente.
 *
 *
 * @startuml title Sequence Diagram of cardinal started expressions
 * CellEditor->CellImp: setContent(String) CellImp->CellImp:storeContent(string)
 * CellImp->FormulaCompiler:FormulaCompiler.getInstance.Compile()
 * FormulaCompiler->ExcellExpressionCompilerCardinal: getStarted() loop
 * FormulaCompiler->FormulaCompiler: compile()
 * FormulaCompiler->ExcellExpressionCompilerCardinal: compile()
 * ExcellExpressionCompilerCardinal->FormulaLexercardinal: create()
 * ExcellExpressionCompilerCardinal->FormulaParserCardinal:
 * formulaParserCardinal() FormulaParserCardinal->ASTFactory: ASTFactory
 * ASTFactory-->FormulaParserCardinal: ASTFactory
 * ExcellExpressionCompilerCardinal->FormulaParserCardinal: expression()
 * FormulaParserCardinal->FormulaParserCardinal:match() note right of
 * FormulaParserCardinal usa o match e o comparison, se for um token conhecido
 * como o :=, entra na class langue corrspondente (no caso Attribution). end
 * note ExcellExpressionCompilerCardinal->ExcellExpressionCompilerCardinal:
 * Convert end loop ExcellExpressionCompilerCardinal-->FormulaCompiler:
 * Expression FormulaCompiler->Formula: Formula() Formula-->CellImp: Formula
 * CellImp->CellImp: ipdateDependences
 * @enduml
 */
public class ExcelExpressionCompilerCardinal implements ExpressionCompiler {

    /**
     * The character that signals that a cell's content is a formula ('#')
     */
    public static final char FORMULA_STARTER = '#';

    /**
     * Creates the Excel expression compiler.
     */
    public ExcelExpressionCompilerCardinal() {
    }

    public char getStarter() {
        return FORMULA_STARTER;
    }

    public Expression compile(Cell cell, String source) throws FormulaCompilationException {
        // Creates the lexer and parser
        FormulaParserCardinal parser = new FormulaParserCardinal(new FormulaLexerCardinal(new StringReader(source)));
        //JOptionPane.showMessageDialog(null, source);
        try {
            // Attempts to match an expression
            parser.expression();
        } catch (ANTLRException e) {
            throw new FormulaCompilationException(e);
        }


        // Converts the expression and returns it
        //JOptionPane.showMessageDialog(null, parser.getAST().toString());
        return convert(cell, parser.getAST());
    }

    /**
     * Converts the given ANTLR AST to an expression.
     *
     * @param node the abstract syntax tree node to convert
     * @return the result of the conversion
     */
    protected Expression convert(Cell cell, AST node) throws FormulaCompilationException {
        // System.out.println("Converting node '" + node.getText() + "' of tree '" + node.toStringTree() + "' with " + node.getNumberOfChildren() + " children.");
        
        if(node.getType()==FormulaParserCardinalTokenTypes.SEMI){
            List<Expression> args = new ArrayList<Expression>();
            AST child = node.getFirstChild();
            if (child != null) {
                args.add(convert(cell, child));
                while ((child = child.getNextSibling()) != null) {
                    args.add(convert(cell, child));
                }
            }
            for (Expression expression : args) {
                try {                
                    expression.evaluate();
                } catch (IllegalValueTypeException ex) {
                    Logger.getLogger(ExcelExpressionCompilerCardinal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return args.get(args.size()-1);
                
        }
            
        if (node.getNumberOfChildren() == 0) {
            try {
                switch (node.getType()) {
                    case FormulaParserCardinalTokenTypes.NUMBER:
                        return new Literal(Value.parseNumericValue(node.getText()));
                    case FormulaParserCardinalTokenTypes.STRING:
                        return new Literal(Value.parseValue(node.getText(), Value.Type.BOOLEAN, Value.Type.DATE));
                    case FormulaParserCardinalTokenTypes.CELL_REF:
                        return new CellReference(cell.getSpreadsheet(), node.getText());
                    case FormulaParserCardinalTokenTypes.NAME:
                    /* return cell.getSpreadsheet().getWorkbook().
                     getRange(node.getText()) (Reference)*/
                }
            } catch (ParseException e) {
                throw new FormulaCompilationException(e);
            }
        }

        // Convert function call
        Function function = null;
        try {
            function = Language.getInstance().getFunction(node.getText());
        } catch (UnknownElementException e) {
        }

        if (function != null) {
            List<Expression> args = new ArrayList<Expression>();
            AST child = node.getFirstChild();
            if (child != null) {
                args.add(convert(cell, child));
                while ((child = child.getNextSibling()) != null) {
                    args.add(convert(cell, child));
                }
            }
            Expression[] argArray = args.toArray(new Expression[args.size()]);
            return new FunctionCall(function, argArray);
        }

        if (node.getNumberOfChildren() == 1) // Convert unary operation
        {
            return new UnaryOperation(
                    Language.getInstance().getUnaryOperator(node.getText()),
                    convert(cell, node.getFirstChild()));
        } else if (node.getNumberOfChildren() == 2) {
            // Convert binary operation
            BinaryOperator operator = Language.getInstance().getBinaryOperator(node.getText());
            if (operator instanceof RangeReference) {
                return new ReferenceOperation(
                        (Reference) convert(cell, node.getFirstChild()),
                        (RangeReference) operator,
                        (Reference) convert(cell, node.getFirstChild().getNextSibling()));
            } else {
                return new BinaryOperation(
                        convert(cell, node.getFirstChild()),
                        operator,
                        convert(cell, node.getFirstChild().getNextSibling()));
            }
        } else // Shouldn't happen
        {
            throw new FormulaCompilationException();
        }
    }
}