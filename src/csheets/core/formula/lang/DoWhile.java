package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.*;

/**
 * A function that runs a while loop according this sequence:
 * #dowhile{expression1;expression2;...;expressionN;condition}
 * performing all the expressions while the condition is true
 * @author Scare
 */
public class DoWhile implements Function {
    /** A boolean variable to prevent infinite loops */
    private static boolean inIteration = false;
    
    /** The function receives 2 parameters: a boolean condition
     * and an uncertain number of expressions to perform */
    public static final FunctionParameter[] parameters = new FunctionParameter[]{
        new FunctionParameter(Value.Type.UNDEFINED, "Expressions", false, "Expression to realize if the condition is true"),
        new FunctionParameter(Value.Type.BOOLEAN, "Condition", false, "A condition to evaluate before proceeding")
    };
    
    /** Creates a new instance of WhileDo function */
    public DoWhile() {
    }

    @Override
    public String getIdentifier() {
        return "DOWHILE";
    }
    
    @Override
    public Value applyTo(Expression[] args) throws IllegalValueTypeException {
        Value val = new Value();

        if (!inIteration) {
            inIteration = true;
            int size = args.length-1;
            while (args[size].evaluate().toBoolean()) {
                for (int i = 0; i < size; i++) {
                    val = args[i].evaluate();
                }
            }
            
            inIteration = false;
        }
        return val;
    }

    @Override
    public FunctionParameter[] getParameters() {
        return parameters;
    }

    @Override
    public boolean isVarArg() {
        return true;
    }
}
