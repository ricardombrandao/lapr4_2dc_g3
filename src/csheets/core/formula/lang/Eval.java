package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;
import csheets.core.formula.Function;
import csheets.core.formula.FunctionParameter;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.core.formula.util.ExpressionVisitor;
import csheets.ui.ctrl.UIController;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class Eval implements Function {
    //Prevent infinite loops
    private static boolean inIteration = false;

    public Eval() {
    }
    
    public static final FunctionParameter[] parameters = new FunctionParameter[]{
        new FunctionParameter(Value.Type.UNDEFINED, "Expressions", false, "Expression to realize if the condition is true")
    };
    
    @Override
    public String getIdentifier() {
        return "EVAL";
    }

    @Override
    public Value applyTo(Expression[] args) throws IllegalValueTypeException{
        //left esquerda
        //right direita
        
        //N expressões
        Value val = new Value();

        if (!inIteration) {
            inIteration = true;
            int size = args.length;
            for (int i = 0; i < size; i++) {
                val = args[i].evaluate();
            }
            inIteration = false;
        }
            
            
        return val;
    }

    @Override
    public FunctionParameter[] getParameters() {
        return parameters;
    }

    @Override
    public boolean isVarArg() {
        return true;
    }
}