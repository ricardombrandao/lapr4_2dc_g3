package csheets.core.formula.lang;

import csheets.core.IllegalValueTypeException;
import csheets.core.Value;
import csheets.core.formula.BinaryOperator;
import csheets.core.formula.Expression;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ui.ctrl.UIController;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe onde é atribuido o valor a celula actual e passado o valor para a de
 * destino. É recebido o leftOperand ("parte que fica antes do :=") e o
 * rightoperand(parte depois do :=). A parte da esquerda é imediatamente passada
 * para o retorno onde o setContenten da class cellImp tratara de colocar no
 * local, já a colocação na celula destino, Como o programa até esta altura não
 * estava preparado para ser ter operações deste genero, tive de fazer a propria
 * atribuição nesta classe. Para ser reconhecida esta nova operação para alem de
 * ser criado o seu funcionamento nest classe foi tambem adicionado o seu nome
 * no language.props que ira procurar pelo token ATT(definido na linguagem como
 * :=) e que ira fazer match com o getIdentifier() desta classe <img src
 * ="../documentation/Luis Barbosa/Core/program_start.jpg" />
 *
 */
public class Attribution implements BinaryOperator {
    //Prevent infinite loops
    private static boolean inIteration = false;

    public Attribution() {
    }

    public Value applyTo(Expression leftOperand, Expression rightOperand) throws IllegalValueTypeException {

        System.out.println("celula: " + leftOperand.toString());
        //não se pode verificar se a celula activa é a mesma que queremos atribuir (para impedir o circle), porque é necessario uma instancia da uiController para retornar a active cell. (Motivos paar não o fazer, modelo de negocio chamar a ui. Não poder modificar as classes já criadas.)
        if (!inIteration) {
            inIteration = true;
            CellReference cellRef = (CellReference) leftOperand;
            try {
                cellRef.getCell().setContent(rightOperand.evaluate().toString());
            } catch (FormulaCompilationException ex) {
                Logger.getLogger(Attribution.class.getName()).log(Level.SEVERE, null, ex);
            }
            inIteration = false;
        }

        return new Value(rightOperand.evaluate().toDouble());
    }

    public String getIdentifier() {
        return ":=";
    }

    public Value.Type getOperandValueType() {
        return Value.Type.NUMERIC;
    }

    public String toString() {
        return getIdentifier();
    }
}