/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;

/**
 *
 * @author Luis Mendes
 */
@PersistenceUnit
public class PersistenceInit {
    
    
   public static EntityManager init()
    {
      EntityManagerFactory factory = Persistence.createEntityManagerFactory("cleansheetsPU");
      EntityManager manager = factory.createEntityManager();
      return manager;
    }
}
