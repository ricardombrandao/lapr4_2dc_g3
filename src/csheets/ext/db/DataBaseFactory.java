/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db;

/**
 *
 * Factory que trata todas as conexoes com a base de dados e todo o codigo que é gerado para lá inserir
 */
public class DataBaseFactory {
    
    private Object DataBase;
   
    public DataBaseFactory(String driver){
        if(driver.equals("HSQLDB"))
        {
            DataBaseUtilHSQLDB HSQL= DataBaseUtilHSQLDB.getInstance();
            this.DataBase=HSQL;
        }
        
        if(driver.equals("DERBY"))
        {
            DataBaseUtilDERBY DERBY= DataBaseUtilDERBY.getInstance();
            this.DataBase=DERBY;
        }
        
        //Possiblidade de implementar mais DB no futuro
        
    }
    
    public Object getObjectDriver(){
        return DataBase;
    }
}
