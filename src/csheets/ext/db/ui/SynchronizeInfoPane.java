/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db.ui;

import csheets.core.Cell;
import csheets.ext.db.ImportExportSyncConf;
import csheets.ext.db.SynchronizeThread;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import csheets.ui.ctrl.UIController;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JComboBox;
import javax.swing.JLabel;

/**
 *
 * Este JDialog pedir os dados ao utilizador para se conectar á base de dados
 */
public class SynchronizeInfoPane extends JDialog {

    private JComboBox comboDB;
    public int nLinhasSelect = 0;
    public Cell[][] celulas;
    UIController uiController;
    private String nameDB;
    private String typeDB;
    private int timeDB;
    private String[][] dadosCombo;
    private String option;

    public SynchronizeInfoPane(int alt, Cell[][] cel, UIController ui) {
        super();
        this.setTitle("Select DataBase");
        this.setLayout(new BorderLayout());
        System.out.println("Visual grafico");

        //necessario para buscar o workbook atual
        uiController = ui;
        // matriz de celulas seleccionadas
        celulas = cel;
        //validação se tem celulas seleccionadas
        nLinhasSelect = alt;

        // paineis paar o leyout
        JPanel painelCentro = new JPanel();
        JPanel painelTop = new JPanel();
        JPanel painelDown = new JPanel();
        // adicionar paineis
        this.add(painelCentro, BorderLayout.CENTER);
        this.add(painelDown, BorderLayout.SOUTH);
        this.add(painelTop, BorderLayout.NORTH);


        JLabel title = new JLabel("Select Data Base");
        painelTop.add(title);

        /////////////////////////////////////////////////////////////////////////////////////
        ///////////////// botões ///////////////////////////////////////////////////////////
        JButton btCancel = new JButton("Cancel");
        btCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                SynchronizeInfoPane.this.dispose();
            }
        });
        
        ///////////////////////////////////////////////////////////////////////////////////
        // botões
        JButton btSync = new JButton("Synchronize");
        btSync.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                typeDB = dadosCombo[comboDB.getSelectedIndex()][1];
                timeDB = Integer.parseInt(dadosCombo[comboDB.getSelectedIndex()][2]);
                nameDB = dadosCombo[comboDB.getSelectedIndex()][0];
                option = dadosCombo[comboDB.getSelectedIndex()][3];
                //System.out.println("type dbXML: "+typeDB);
                //System.out.println("name dbXML: "+nameDB);
                SynchronizeThread sy = new SynchronizeThread(uiController, typeDB, nameDB, timeDB, celulas,option);
                Thread thread = new Thread(sy);
                SynchronizeInfoPane.this.dispose();
                thread.start();
                // inicia a thread na classse

            }
        });
        painelDown.add(btSync);
        painelDown.add(btCancel);
        /////////////////////////////////////////////////////////////////////////////////
        try {
            ImportExportSyncConf teste = new ImportExportSyncConf();
            dadosCombo = teste.readXML();
        } catch (Exception e) {
            System.out.println("Erro ao ler xml");
        }
        //ComboBox
        comboDB = new JComboBox();
        //comboDB.setMaximumRowCount(2); //numero maximo de linhas
        for (int i = 0; i < dadosCombo.length; i++) {
            comboDB.addItem(dadosCombo[i][0]);
        }
        comboDB.addItem("criar..");
        comboDB.setSelectedIndex(0);
        painelCentro.add(comboDB);

        comboDB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               // System.out.println("select: " + comboDB.getSelectedIndex());
               // System.out.println("max: " + comboDB.getItemCount());
                // caso carregue em criar que é a ultima opção e tenha pelo menos uma celula seleccionada
                if (comboDB.getItemCount() - 1 == comboDB.getSelectedIndex()) {
                    if (nLinhasSelect < 1) {
                        JOptionPane.showMessageDialog(null, "Para ser considerado uma tabela\nTem de ter selecionado no mínimo 2 linhas");
                    } else {
                        System.out.println("Passou: " + nLinhasSelect + " celulas");
                        SynchronizeCreatorInfoPane pp = new SynchronizeCreatorInfoPane(celulas);
                        SynchronizeInfoPane.this.dispose();
                    }

                } else {
                    nameDB=dadosCombo[comboDB.getSelectedIndex()][0];
                    typeDB=dadosCombo[comboDB.getSelectedIndex()][1];
                    timeDB= Integer.parseInt(dadosCombo[comboDB.getSelectedIndex()][2]);
                
                }
            }
        });




        pack();
        setMinimumSize(new Dimension(getSize().width, getSize().height));

        Dimension dim_ecra = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((dim_ecra.width) / 2 - (this.getWidth() / 2), (dim_ecra.height) / 2 - (this.getHeight() / 2));

        setVisible(true);

    }
}