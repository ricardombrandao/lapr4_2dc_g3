/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db.ui;

import csheets.core.Cell;
import csheets.ext.db.DBXmlLoader;
import csheets.ext.db.DataBaseController;
import csheets.ext.db.ImportExportSyncConf;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Luis Mendes
 */
public class SynchronizeCreatorInfoPane extends JDialog {

    private JComboBox comboDB;
    private JComboBox comboOption;
    private Cell[][] celulas;
    private JTextField jtextName;
    private String typeDB;
    ArrayList<String> drivers;
    JTextField jtextTime;
    private String[][] dadosXml;

    public SynchronizeCreatorInfoPane(Cell[][] cel) {
        super();
        this.setTitle("Create new DataBase");
        this.setLayout(new BorderLayout());

        JPanel painelCentro = new JPanel();
        JPanel painelTop = new JPanel();
        JPanel painelDown = new JPanel();

        this.add(painelCentro, BorderLayout.CENTER);
        this.add(painelDown, BorderLayout.SOUTH);
        this.add(painelTop, BorderLayout.NORTH);


        celulas = cel;

        JLabel title = new JLabel("Create new Data Base");
        painelTop.add(title);

        JPanel painelGrid = new JPanel(new GridLayout(0, 1));

        JPanel linha1 = new JPanel();
        JPanel linha2 = new JPanel();
        JPanel linha3 = new JPanel();
        JPanel linha4 = new JPanel();
        ///////////////////////////////////////////////////////////////////////
        JLabel labelName = new JLabel("Nome Sync: ");
        jtextName = new JTextField(20);
        jtextName.setSize(30, 40);
        
        linha1.add(labelName);
        linha1.add(jtextName);
        //////////////////////////////////////////////////////////////////////


        /////////////////////////// linha 2 //////////////////////////////////////
        JLabel txt1 = new JLabel("Tempo de Sync: ");
        jtextTime = new JTextField(20);


        linha2.add(txt1);
        linha2.add(jtextTime);
        /////////////////////////////////////////////////////////////////////////////
        painelGrid.add(linha1);
        painelGrid.add(linha2);
        painelCentro.add(painelGrid);


        ////////////////////////////////////////////////////////////////////////////
        // botões
        JButton btSync = new JButton("Create new");


        btSync.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String nomeTabela = jtextName.getText().toString();

                DataBaseController bb = new DataBaseController();
                bb.DataBaseSyncCreator(nomeTabela, typeDB, celulas);
                System.out.println("Entrou com : " + nomeTabela + " , " + typeDB);

                ///////////////////////////////////////////////////////////////////////////
                // export new database for xml
                ImportExportSyncConf sy = new ImportExportSyncConf();
                try {
                    dadosXml = sy.readXML();
                    int max = 0;
                    int col = 0;
                    for (String[] line : dadosXml) {
                        max++;
                        for (String s : line) {
                            //System.out.println("res: " + s);
                        }
                    }

                    String dados[][] = new String[max + 2][4];
                    max = 0;
                    for (String[] line : dadosXml) {
                        col = 0;
                        for (String s : line) {
                            dados[max][col] = dadosXml[max][col];
                            //System.out.println(dados[max][0]);
                            col++;
                        }
                        max++;
                    }
                    // adiciona o novo elemento
                    dados[max][0] = nomeTabela;
                    dados[max][1] = typeDB;
                    dados[max][2] = jtextTime.getText();
                    dados[max][3] = (String)comboOption.getSelectedItem();
                    //System.out.println("Valor select: "+dados[max][3]);
                    //System.out.println(dados[max][0]);

                    sy.writeXML(dados, max + 1);
                    SynchronizeCreatorInfoPane.this.dispose();
                    
                } catch (Exception ex) {
                    System.out.println("Erro no imort / export");
                }
            }
        });

        painelDown.add(btSync);

        JButton btCancel = new JButton("Cancel");
        btCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                final Object[] options = {"Sim", "Não"};
                int i = JOptionPane.showOptionDialog(null, "Vai perder todos os dados\nTem a certeza que deseja cancelar?", "Cancelar", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                if (i == JOptionPane.YES_OPTION) {
                    SynchronizeCreatorInfoPane.this.dispose();
                } else if (i == JOptionPane.NO_OPTION) {
                    SynchronizeCreatorInfoPane.this.repaint();
                }
            }
        });

        painelDown.add(btCancel);
        ///////////////////////////////////////////////////////////
        // adicionar tipo de DB
        JLabel comboText = new JLabel("Driver: ");
        comboDB = new JComboBox();
        DBXmlLoader loader = DBXmlLoader.getInstance();

        try {
            drivers = loader.getDBList();
        } catch (Exception e) {
            drivers = null;
        }

        if (drivers != null) {
            for (int i = 0; i < drivers.size(); i++) {
                comboDB.addItem(drivers.get(i).toString());
            }
        } else {
            comboDB.addItem("");
            // chama erro
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        //ComboBox

        comboDB.setSelectedIndex(0);
        typeDB = drivers.get(comboDB.getSelectedIndex());

        linha3.add(comboText);
        linha3.add(comboDB);

        comboDB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                typeDB = drivers.get(comboDB.getSelectedIndex());
            }
        });

        painelGrid.add(linha3);

        ///////////////////////////////////////////////////////////////////////////////////////////////////
        //
         JLabel comboText2 = new JLabel("Option: ");
        comboOption = new JComboBox();
        comboOption.addItem("WorkBook");
        comboOption.addItem("Database");
        comboOption.setSelectedIndex(0);
        
        linha4.add(comboText2);
        linha4.add(comboOption);

        comboOption.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String abc = (String)comboOption.getSelectedItem();
                System.out.println(abc);
            }
        });

        painelGrid.add(linha4);
        
        
        
        
        
        pack();
        setMinimumSize(new Dimension(getSize().width, getSize().height));

        Dimension dim_ecra = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((dim_ecra.width) / 2 - (this.getWidth() / 2), (dim_ecra.height) / 2 - (this.getHeight() / 2));

        setVisible(true);

    }
}
