/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db.ui;

import javax.swing.JOptionPane;

/**
 *
 * @author Scare
 */
public class PainelMessage{
    
    public PainelMessage(){
    }
        
    public void PainelOk(String mensagem){
        JOptionPane.showMessageDialog(null, mensagem);
    }
    
    public boolean PainelYesNo(String mensagem){
       final Object[] options = {"Sim", "Não"};
       //"Tabela Existe, Deseja escrever por cima?"
       int i = JOptionPane.showOptionDialog(null, mensagem, "INFO", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
       if (i == JOptionPane.YES_OPTION) {return true;}
       if (i == JOptionPane.NO_OPTION){return false;}
       return true;
    }
    
}
