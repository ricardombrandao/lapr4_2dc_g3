/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db.ui;

import csheets.core.Cell;
import csheets.ext.db.DBXmlLoader;
import csheets.ext.db.DataBaseController;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import csheets.ui.ctrl.UIController;
import java.io.File;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;

/**
 *
 * Este JDialog pedir os dados ao utilizador para se conectar á base de dados
 */
public class DataBaseInfoPane extends JDialog{
    private Cell[][] cel;
    private JTextField txNomeDB;
    private JTextField txUsername;
    private JPasswordField txPassword;
    private JTextField txTabela;
    private JTextField txCells;
    private JComboBox comboDB;
    private UIController ui;
    private JList listaPK;
    
    public DataBaseInfoPane(Cell[][] celulas,UIController uic,String nomeDB, String username, String password, String tabela){
        super();
        this.setTitle("Registar Base Dados");
        
        cel=celulas;
        ui=uic;
        
        

        this.setLayout(new BorderLayout());
        
        JPanel painelCentro = new JPanel();
        JPanel painelTop = new JPanel();
        JPanel painelDown = new JPanel();
        
        this.add(painelCentro,BorderLayout.CENTER);
        this.add(painelDown,BorderLayout.SOUTH);
        this.add(painelTop,BorderLayout.NORTH);
        
        JButton btExport = new JButton("Exportar");
        DataBaseInfoPane.TrataEventoBtRegistar eventoRegistar = new DataBaseInfoPane.TrataEventoBtRegistar();
        btExport.addActionListener(eventoRegistar);
        painelDown.add(btExport);
        
        JButton btCancel = new JButton("Cancelar");
        DataBaseInfoPane.TrataEventoBtCancel eventoCancel = new DataBaseInfoPane.TrataEventoBtCancel();
        btCancel.addActionListener(eventoCancel);
        painelDown.add(btCancel);
        
        JButton btDB = new JButton("...");
        DataBaseInfoPane.TrataEventoBtDB eventoDB = new DataBaseInfoPane.TrataEventoBtDB();
        btDB.addActionListener(eventoDB);
        
//        JButton btCells = new JButton("...");
//        DataBaseInfoPane.TrataEventoBtCells eventoCells = new DataBaseInfoPane.TrataEventoBtCells();
//        btCells.addActionListener(eventoCells);
        
        JLabel lbTitulo = new JLabel("Registar Base Dados");
        Font currentFont = lbTitulo.getFont();
        lbTitulo.setFont(new Font("", currentFont.getStyle(), 17));
        painelTop.add(lbTitulo);
        
        JLabel[] labels = new JLabel[7];
        labels[0] = new JLabel("Driver Base Dados:",SwingConstants.RIGHT);
        labels[1] = new JLabel("Nome Base Dados:",SwingConstants.RIGHT);
        labels[2] = new JLabel("Username:",SwingConstants.RIGHT);
        labels[3] = new JLabel("Password:",SwingConstants.RIGHT);
        labels[4] = new JLabel("Nome Nova Tabela:",SwingConstants.RIGHT);
        labels[5] = new JLabel("Células a Exportar:",SwingConstants.RIGHT);
        labels[6] = new JLabel("Escolha as PK:",SwingConstants.RIGHT);
        
        this.setModal(true);
        
        int maxWidth = 0, maxHeight = 0;
        for(JLabel lbl : labels) {
            int w = lbl.getPreferredSize().width;
            int h = lbl.getPreferredSize().height;
            if(maxWidth < w)
                maxWidth = w;
            if(maxHeight < h)
                maxHeight = h;
        }
        
        btDB.setPreferredSize(new Dimension(20,maxHeight));
        
        for(JLabel lbl : labels)
            lbl.setPreferredSize(new Dimension(maxWidth, maxHeight));
        
        
        
        //txEndDB = new JTextField(20);
        comboDB=new JComboBox();
        
        txNomeDB = new JTextField(18);
        txUsername = new JTextField(20);
        txPassword = new JPasswordField(20);
        txTabela = new JTextField(20);
        txCells = new JTextField(20);
        
        //Colocar o valor das celulas escolhidas neste género (=Folha1!$B$1:$D$5)
        txNomeDB.setEditable(false);
        txCells.setEditable(false);
        
        
        txCells.setText(expressaoCells());
        
        DefaultListModel listModel = new DefaultListModel();
        listaPK = new JList(listModel);
        ArrayList<String> colunas = devolveColunasTabela(cel);
        listaPK.setVisibleRowCount(3);
        JScrollPane listScroller = new JScrollPane(listaPK);
        listaPK.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        //listScroller.setPreferredSize(new Dimension(250, 40));
        
        for(int i=0; i<colunas.size();i++){
            listModel.add(i, colunas.get(i).toString());
        }
        
        JPanel painelEndDB = new JPanel(new FlowLayout(FlowLayout.LEFT));
        painelEndDB.add(labels[0]);
        painelEndDB.add(comboDB);
        
        JPanel painelNomeDB = new JPanel(new FlowLayout(FlowLayout.LEFT));
        painelNomeDB.add(labels[1]);
        painelNomeDB.add(txNomeDB);
        painelNomeDB.add(btDB);
        
        JPanel painelUsername = new JPanel(new FlowLayout(FlowLayout.LEFT));
        painelUsername.add(labels[2]);
        painelUsername.add(txUsername);
        
        JPanel painelPassword = new JPanel(new FlowLayout(FlowLayout.LEFT));
        painelPassword.add(labels[3]);
        painelPassword.add(txPassword);
        
        JPanel painelTabela = new JPanel(new FlowLayout(FlowLayout.LEFT));
        painelTabela.add(labels[4]);
        painelTabela.add(txTabela);
        
        JPanel painelCells = new JPanel(new FlowLayout(FlowLayout.LEFT));
        painelCells.add(labels[5]);
        painelCells.add(txCells);
//        painelCells.add(btCells);
        
        JPanel painelList = new JPanel(new FlowLayout(FlowLayout.LEFT));
        painelList.add(labels[6]);
        painelList.add(listScroller);
        
        JPanel painelGrid = new JPanel(new GridLayout(0,1));
        
        painelGrid.add(painelEndDB);
        painelGrid.add(painelNomeDB);
        painelGrid.add(painelUsername);
        painelGrid.add(painelPassword);
        painelGrid.add(painelTabela);
        painelGrid.add(painelCells);
        painelGrid.add(painelList);
       
        painelCentro.add(painelGrid);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        txNomeDB.setText(nomeDB);
        txUsername.setText(username);
        txPassword.setText(password);
        txTabela.setText(tabela);
        
        
        DBXmlLoader loader = DBXmlLoader.getInstance();
        
        ArrayList<String> drivers;
        try{
            drivers = loader.getDBList();
        }catch(Exception e){
            drivers=null;
        }
        
        if(drivers!=null){
            for(int i=0;i<drivers.size();i++){
                comboDB.addItem(drivers.get(i).toString());
            }
        }else{
            comboDB.addItem("");
        }
        
        
        pack();
        setMinimumSize(new Dimension(getSize().width,getSize().height));
        
        Dimension dim_ecra = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((dim_ecra.width) / 2 - (this.getWidth() / 2), (dim_ecra.height) / 2 - (this.getHeight() / 2));
                
        setVisible(true);
    }
    
    private ArrayList<String> devolveColunasTabela(Cell[][] cel){
        //Armazena nomes das colunas na arraylist
        ArrayList<String> colunas = new ArrayList<String>();
        int cont=0;
        for (Cell[] row : cel) {
            for (Cell cell : row) {
                cont++;
                if(cell.getValue().toString().equals("")){
                    colunas.add("Column"+cont);}
                else{
                    colunas.add(cell.getValue().toString());}
            }
            break;
        }
        //System.out.println("Numero de colunas: "+colunas.size());
        return colunas;
    }
    
    private String expressaoCells(){
        String nomeFolha = ui.getActiveSpreadsheet().getTitle();
        Cell primeiraCelula=null;
        Cell ultimaCelula=null;
        int contador=0;
        for (Cell[] row : cel) {
            for (Cell cell : row) {
                if(contador==0)
                    primeiraCelula=cell;
                else
                    ultimaCelula=cell;
                contador++;
            }
        }
        int coluna1 = primeiraCelula.getAddress().getColumn();
        int linha1 = primeiraCelula.getAddress().getRow()+1;
        int coluna2 = ultimaCelula.getAddress().getColumn();
        int linha2 = ultimaCelula.getAddress().getRow()+1;
        String textoCelulas = "="+nomeFolha+"!$"+ColumnNumberToChar(coluna1)+"$"+linha1+":$"+ColumnNumberToChar(coluna2)+"$"+linha2;
        return textoCelulas;
    }
    
    private char ColumnNumberToChar(int a){
        
        return (char)((int)'A'+a);
    }
    
    private String textNomeDB="",textUsername="",textPassword="",textTabela="",textCombo="";
    private int[] indexes = null;
    
    class TrataEventoBtRegistar implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if(comboDB.getSelectedItem().toString().equals("")){
                JOptionPane.showMessageDialog(rootPane, "Tem de colocar o driver da Base de Dados");
                DataBaseInfoPane.this.repaint();
                return;
            }else{
                textCombo = comboDB.getSelectedItem().toString();
            }
            
            if(txNomeDB.getText().equals("")){
                JOptionPane.showMessageDialog(rootPane, "Tem de colocar o nome da Base de Dados");
                DataBaseInfoPane.this.repaint();
                return;
            }else{
                textNomeDB = txNomeDB.getText();
            }
            if(txUsername.getText().equals("")){
                JOptionPane.showMessageDialog(rootPane, "Tem de colocar o username para login na Base de Dados");
                DataBaseInfoPane.this.repaint();
                return;
            }else{
                textUsername = txUsername.getText();
            }    
            //if(txPassword.getText().equals("")){
            //    JOptionPane.showMessageDialog(rootPane, "Tem de colocar a password para login na Base de Dados");
            //    DataBaseInfoPane.this.repaint();
            //    return;
            //}else{
                char[] charPass = txPassword.getPassword();
                for(int i=0;i<charPass.length;i++)
                    textPassword=textPassword + charPass[i];
                //textPassword = txPassword.getText();
            //}
            if(txTabela.getText().equals("")){
                JOptionPane.showMessageDialog(rootPane, "Tem de colocar o nome da Tabela da Base de Dados");
                DataBaseInfoPane.this.repaint();
                return;
            }else{
                textTabela = txTabela.getText();
                textTabela = textTabela.toUpperCase();
            }
            
            
            indexes = listaPK.getSelectedIndices();
            if(indexes==null){
                JOptionPane.showMessageDialog(null, "Não vai inserir chaves primárias na tabela");
            }
            
            //Antes de chamar isto iniciar a thread
            runThread t = new runThread();
            t.start();
            
            DataBaseInfoPane.this.dispose();
        }
    }
    
    class runThread extends Thread{
        @Override
        public void run(){
            DataBaseController dbControl = new DataBaseController();
            try {
                dbControl.DataBaseExport(textCombo, cel, textNomeDB, textUsername, textPassword, textTabela, ui, indexes);
            } catch (Exception ex) {
                System.out.println(ex.getCause());
            }
        }
    }
    
    class TrataEventoBtCancel implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            final Object[] options = {"Sim", "Não"};
            int i = JOptionPane.showOptionDialog(null, "Vai perder todos os dados\nTem a certeza que deseja cancelar?", "Cancelar", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
            if (i == JOptionPane.YES_OPTION) {
               DataBaseInfoPane.this.dispose();
            } else if (i == JOptionPane.NO_OPTION) {
               DataBaseInfoPane.this.repaint();
            }  
        }
    }
    
    class TrataEventoBtDB implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            JFileChooser chooser;
            chooser = new JFileChooser();
            String caminho = "";
            
            comboDB.setEnabled(false);
            if(comboDB.getSelectedItem().toString().equals("DERBY")){
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                chooser.setAcceptAllFileFilterUsed(false);
            }
            
            int retorno = chooser.showOpenDialog(null);
            if (retorno==JFileChooser.APPROVE_OPTION){
                    caminho = chooser.getSelectedFile().getAbsolutePath();  // o getSelectedFile pega o arquivo e o getAbsolutePath retorna uma string contendo o endereço.
            }  
            
            File file;
            file = new File(caminho);
            
            if(file.getPath().equals("")){
                txNomeDB.setText("");
                txUsername.setEditable(true);
                txPassword.setEditable(true);
                return;
            }
            
            int last=0;
            for(int c=0;c<caminho.length();c++){
                if(caminho.charAt(c)=='.')
                 last=c;
            }
            //String caminhoFicheiro = caminho.substring(0, last);
            int depoisPonto=last;
            if(last!=0){
            depoisPonto++;
            }
            String extensaoFicheiro = caminho.substring(depoisPonto, caminho.length());
            
            if( (!extensaoFicheiro.equals("script")) && (!extensaoFicheiro.equals("properties")) ){
            if(last>0){
                txNomeDB.setText("");
                System.out.println("Extensão ficheiro não é válido");
                PainelMessage erroFileNotFound = new PainelMessage();
                erroFileNotFound.PainelOk("Extensão ficheiro não é válido");
                return;
            }}
            
            File file2;
            file = new File(caminho);
            file2 = new File(caminho+".script");
            if(file.exists()){
                String caminhoFich = caminho;      
                if(last>0){
                caminhoFich = caminho.substring(0, last);
                }
                txNomeDB.setText(caminhoFich);
                txUsername.setEditable(true);
                txPassword.setEditable(true);
                System.out.println("Ficheiro existe");  
            }else if(file2.exists()){
                txNomeDB.setText(caminho);
                txUsername.setEditable(true);
                txPassword.setEditable(true);
                System.out.println("Ficheiro existe");
            } else {
                if(comboDB.getSelectedItem().toString().equals("HSQLDB")){
                txNomeDB.setText(caminho);
                txUsername.setEditable(false);
                txPassword.setEditable(false);
                txUsername.setText("SA");
                txPassword.setText("");
                System.out.println("Ficheiro tem de ser criado");}
                else if(comboDB.getSelectedItem().toString().equals("DERBY")){
                txNomeDB.setText(caminho);
                txUsername.setEditable(false);
                txPassword.setEditable(false);
                txUsername.setText("SA");
                txPassword.setText("manager");
                System.out.println("Ficheiro tem de ser criado");}
            }
        }
    }
}
