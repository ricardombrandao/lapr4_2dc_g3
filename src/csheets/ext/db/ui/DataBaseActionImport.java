package csheets.ext.db.ui;

import csheets.core.Cell;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;

/**
 * An action of the simple extension that exemplifies how to interact with the spreadsheet.
 * @author Alexandre Braganca
 */
public class DataBaseActionImport extends FocusOwnerAction{

	/** The user interface controller */
	protected UIController uiController;

	/**
	 * Creates a new action.
	 * @param uiController the user interface controller
	 */
	public DataBaseActionImport(UIController uiController) {
		this.uiController = uiController;
	}

	protected String getName() {
		return "Importar";
	}

	protected void defineProperties() {
	}
        

	/**
	 * A simple action that presents a confirmation dialog.
	 * If the user confirms then the contents of the cell A1 of the current sheet are set to the string "Changed".
	 * @param event the event that was fired
	 */
        @Override
	public void actionPerformed(ActionEvent event) {
		try {
                    
                    Cell cel = uiController.getActiveCell();
                    DataBaseImportPane janela = new DataBaseImportPane(cel,uiController,"","","","");
		} catch (Exception ex) {
			// para ja ignoramos a excepcao
		}
	}
}
