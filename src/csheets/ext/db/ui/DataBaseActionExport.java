package csheets.ext.db.ui;

import csheets.core.Cell;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;

/**
 * An action of the simple extension that exemplifies how to interact with the spreadsheet.
 * @author Alexandre Braganca
 */
public class DataBaseActionExport extends FocusOwnerAction{

	/** The user interface controller */
	protected UIController uiController;

	/**
	 * Creates a new action.
	 * @param uiController the user interface controller
	 */
	public DataBaseActionExport(UIController uiController) {
		this.uiController = uiController;
	}

	protected String getName() {
		return "Exportar";
	}

	protected void defineProperties() {
	}
        

	/**
	 * A simple action that presents a confirmation dialog.
	 * If the user confirms then the contents of the cell A1 of the current sheet are set to the string "Changed".
	 * @param event the event that was fired
	 */
        @Override
	public void actionPerformed(ActionEvent event) {
		try {
                    
                    Cell[][] celulas = focusOwner.getSelectedCells();
                    int alt=0, larg=0;
                    alt=celulas.length;
                    for (Cell[] row : celulas){
                        for (Cell cell : row) {
                            larg++;
                        }
                        break;
                    }
                    
                    //System.out.println("Altura="+alt);
                    //System.out.println("Largura="+larg);
                    if(alt<2){
                        JOptionPane.showMessageDialog(null, "Para ser considerado uma tabela\nTem de ter selecionado no mínimo 2 linhas");
                    }else{
                    DataBaseInfoPane janela = new DataBaseInfoPane(celulas,uiController,"","","","");
                    }
		} catch (Exception ex) {
			// para ja ignoramos a excepcao
		}
		//}
	}
}
