package csheets.ext.db.ui;

import csheets.core.Cell;
import java.awt.event.ActionEvent;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;


public class DataBaseActionSynchronize extends FocusOwnerAction{

    
    
	/** The user interface controller */
	protected UIController uiController;

	/**
	 * Creates a new action.
	 * @param uiController the user interface controller
	 */
	public DataBaseActionSynchronize(UIController uiController) {
		this.uiController = uiController;
	}

	protected String getName() {
		return "Synchronize";
	}

	protected void defineProperties() {
	}
        

	/**
	 * A simple action that presents a confirmation dialog.
	 * If the user confirms then the contents of the cell A1 of the current sheet are set to the string "Changed".
	 * @param event the event that was fired
	 */
        @Override
	public void actionPerformed(ActionEvent event) {
		try {
                    // aceder as selected cell's
                    Cell[][] celulas = focusOwner.getSelectedCells();
                    int alt=0, larg=0;
                    alt=celulas.length;
                    for (Cell[] row : celulas){
                        for (Cell cell : row) {
                            larg++;
                            System.out.println(cell.getAddress());
                           
                        }
                        break;
                    }
                    SynchronizeInfoPane janela = new SynchronizeInfoPane(alt, celulas, uiController);
                    
                    //System.out.println("Altura="+alt);
                    //System.out.println("Largura="+larg);
                   /*
                    if(alt<2){
                        JOptionPane.showMessageDialog(null, "Para ser considerado uma tabela\nTem de ter selecionado no mínimo 2 linhas");
                    }else{
                        System.out.println("Passou: "+ alt+" celulas");
                        SynchronizeInfoPane janela = new SynchronizeInfoPane(alt);
                    }
                    */
		} catch (Exception ex) {
			// para ja ignoramos a excepcao
		}
		//}
	}
}
