/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db.ui;

import csheets.core.Cell;
import csheets.ext.db.DataBaseFactory;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * Este JDialog vai mostrar os resultados da inserção na Base de dados ao utilizador
 */
public class DataBaseShowResults extends JDialog{
    
    
    public DataBaseShowResults(ResultSet rs, ArrayList<String> colunas){
        
        super();
        setSize(480, 550);
        setLocationRelativeTo(null);
        setTitle("Celulas Inseridas");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new BorderLayout());

        JPanel painelTop = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel painelCenter = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel painelDown = new JPanel(new FlowLayout(FlowLayout.CENTER));

        JButton btOk = new JButton("Ok");
        btOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataBaseShowResults.this.dispose();
            }
        });



        JLabel lbTitulo = new JLabel("Dados Inseridos na Base de Dados");
        Font currentFont = lbTitulo.getFont();
        lbTitulo.setFont(new Font("", currentFont.getStyle(), 17));
              
        int tam = colunas.size();
        
        String[] coluna = new String[tam];
        String[][] dados = {};
        
        for(int i=0;i<tam;i++){
            coluna[i]=colunas.get(i);
        }
        
        DefaultTableModel model = new DefaultTableModel(dados, coluna);
        final JTable table = new JTable(model);
        table.getTableHeader().setResizingAllowed(false); 
        
        try{
        //for....
        ResultSetMetaData meta   = rs.getMetaData();
        int               colmax = meta.getColumnCount();
        int               i;
        Object            o = null;
        

        // the result set is a cursor into the data.  You can only
        // point to one row at a time
        // assume we are pointing to BEFORE the first row
        // rs.next() points to next row and returns true
        // or false if there is no next row, which breaks the loop
        String[] linha = new String[tam];
        for (; rs.next(); ) {
            for (i = 0; i < colmax; ++i) {
                o = rs.getObject(i + 1);    // Is SQL the first column is indexed

                // with 1 not 0
                linha[i]=o.toString();
                //System.out.print(o.toString() + " ");
            }
            Object[] obj = new Object[tam];
            for(int k=0;k<tam;k++){
                obj[k]=linha[k];
            }
            model.insertRow(table.getRowCount(), obj);
            linha=new String[tam];
            //System.out.println(" ");
        }
        } catch(Exception ex){
            //Deu erro
        }
        
        
        JScrollPane scrollpane = new JScrollPane(table);
        
        
        painelTop.add(lbTitulo, BorderLayout.CENTER);
        painelCenter.add(scrollpane);
        painelDown.add(btOk,BorderLayout.CENTER);

        this.add(painelTop, BorderLayout.NORTH);
        this.add(painelCenter, BorderLayout.CENTER);
        this.add(painelDown, BorderLayout.SOUTH);
        
        Dimension d = new Dimension(this.getWidth(),this.getHeight());
        setMinimumSize(d);
        
        this.setAlwaysOnTop(true);
        setVisible(true);
    } 
    
}
