/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author Luis Mendes
 */
public class ImportExportSyncConf {

    private String[][] data;
    private int sizeOnRead = 0;

    public void writeXML(String[][] dat, int size) throws SAXException, IOException {
        try {
            System.out.println("A escrever...");
            data = dat;
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            org.w3c.dom.Document doc = docBuilder.newDocument();
            //Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("DataSync");
            doc.appendChild(rootElement);

            // data_base elements
            // ciclo for
            for (int i = 0; i < size; i++) {
                Element data_base = doc.createElement("data_base");
                rootElement.appendChild(data_base);

                // firstname elements
                Element nameElement = doc.createElement("name");
                nameElement.appendChild(doc.createTextNode(data[i][0]));
                data_base.appendChild(nameElement);
                System.out.println(data[i][0]);

                // typeElement elements
                Element typeElement = doc.createElement("type");
                typeElement.appendChild(doc.createTextNode(data[i][1]));
                //System.out.println(data[i][1]);
                data_base.appendChild(typeElement);

                // timeElement elements
                Element timeElement = doc.createElement("time");
                timeElement.appendChild(doc.createTextNode(data[i][2]));
                data_base.appendChild(timeElement);
                
                // optionElement elements
                Element optionElement = doc.createElement("option");
                optionElement.appendChild(doc.createTextNode(data[i][3]));
                data_base.appendChild(optionElement);
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(".\\syncfiles\\confSync.xml"));


            transformer.transform(source, result);

            System.out.println("File saved!");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }

    }

    public String[][] readXML() throws ParserConfigurationException, SAXException, IOException {
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            //Document doc = docBuilder.parse (new File("book.xml"));
            org.w3c.dom.Document doc = docBuilder.parse(new File(".\\syncfiles\\confSync.xml"));

            // normalize text representation
            doc.getDocumentElement().normalize();
            System.out.println("Root element of the doc is "
                    + doc.getDocumentElement().getNodeName());


            NodeList listOfPersons = doc.getElementsByTagName("data_base");
            int size = listOfPersons.getLength();
            System.out.println("Total no of configs : " + size);

            //matriz
            data = new String[size][4];

            for (int s = 0; s < listOfPersons.getLength(); s++) {


                Node firstPersonNode = listOfPersons.item(s);
                if (firstPersonNode.getNodeType() == Node.ELEMENT_NODE) {


                    Element firstPersonElement = (Element) firstPersonNode;

                    //-------
                    NodeList nodeNameList = firstPersonElement.getElementsByTagName("name");
                    Element nameElement = (Element) nodeNameList.item(0);


                    NodeList textNameList = nameElement.getChildNodes();
                    data[s][0] = ((Node) textNameList.item(0)).getNodeValue().trim();
                    System.out.println("Dados no read: " + data[s][0]);
                    //-------
                    NodeList nodeTypeList = firstPersonElement.getElementsByTagName("type");
                    Element typeElement = (Element) nodeTypeList.item(0);

                    NodeList textTypeList = typeElement.getChildNodes();
                    data[s][1] = ((Node) textTypeList.item(0)).getNodeValue().trim();
                    System.out.println(((Node) textTypeList.item(0)).getNodeValue().trim());
                    //----
                    NodeList nodeTimeList = firstPersonElement.getElementsByTagName("time");
                    Element timeElement = (Element) nodeTimeList.item(0);

                    NodeList textTimeList = timeElement.getChildNodes();
                    data[s][2] = ((Node) textTimeList.item(0)).getNodeValue().trim();
                    
                    NodeList nodeOptionList = firstPersonElement.getElementsByTagName("option");
                    Element optionElement = (Element) nodeOptionList.item(0);

                    NodeList textOptionList = optionElement.getChildNodes();
                    data[s][3] = ((Node) textOptionList.item(0)).getNodeValue().trim();


                    //------

                }


            }

            //writeXML(data, size);

            
        } catch (FileNotFoundException fe) {
           writeXML(data,0);
           readXML();
           return data;
           
      
        } catch (SAXParseException err) {
            System.out.println("** Parsing error" + ", line "
                    + err.getLineNumber() + ", uri " + err.getSystemId());
            System.out.println(" " + err.getMessage());

        } catch (SAXException e) {
            Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();

        } catch (Throwable t) {
            t.printStackTrace();
        }

        return data;




    }
}
