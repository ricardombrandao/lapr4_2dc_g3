/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db;

import csheets.core.Cell;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Scare
 */
public class DataBaseUtilHSQLDB {

    Statement stmt;
    Connection conn;
    private static Logger logger = Logger.getLogger(DataBaseUtilHSQLDB.class.getName());
    private static DataBaseUtilHSQLDB instancia = new DataBaseUtilHSQLDB();

    private DataBaseUtilHSQLDB() {
        logger.setLevel(Level.INFO);
    }

    /**
     * 
     * @return instance
     */
    public static DataBaseUtilHSQLDB getInstance() {
        return instancia;
    }

    /**
     * 
     * @param nomeDB
     * @param username
     * @param password
     * @return true/false
     */
    public boolean ligarBaseDados(String nomeDB, String username, String password) {
        try {
            turnDBOn(nomeDB, username, password);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Erro ao abrir conexâo com a BaseDados", e);
            return false;
        }
        return true;
    }

    /**
     * 
     * @return true/false
     */
    public boolean desligarBaseDados() {
        try {
            turnDBOff();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Erro ao desligar da BaseDados", e);
            return false;
        }
        return true;
    }

    /**
     * 
     * @param tabela
     * @param cels
     * @param drop
     * @param indexes
     * @return 
     */
    public String criarTabela(String tabela, Cell[][] cels, boolean drop, int[] indexes) {
        if (drop == false) {
            ArrayList<String> colunas = devolveColunasTabela(cels);
            boolean tableExists = checkTableExistence(tabela);
            if (tableExists == true) {
                return "NomeTabelaExiste";
            }
            boolean collumnAccept = checkCollumnAcceptable(colunas);
            if (collumnAccept == false) {
                return "NomeColunaInvalido";
            }

            boolean tableAccept = checkNameTableAccepted(tabela);
            if (tableAccept == false) {
                return "NomeTabelaInvalido";
            }

            String createTable = criaStatementTabela(tabela, cels, colunas, indexes);

            try {
                insertQuery(createTable);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Erro ao fazer Create Table", e);
                return "ErroDesconhecido";
            }

            return "TabelaCriada";
            //missing NomeTabelaInvalido
        } else if (drop == true) {
            ArrayList<String> colunas = devolveColunasTabela(cels);

            String dropTable = criaStatementDropTable(tabela);
            try {
                insertQuery(dropTable);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Erro ao fazer Drop Table", e);
                return "ErroDesconhecido";
            }

            boolean collumnAccept = checkCollumnAcceptable(colunas);
            if (collumnAccept == false) {
                return "NomeColunaInvalido";
            }

            String createTable = criaStatementTabela(tabela, cels, colunas, indexes);

            try {
                insertQuery(createTable);
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Erro ao fazer Create Table", e);
                return "ErroDesconhecido";
            }

            return "TabelaCriada";
            //missing NomeTabelaInvalido
        }
        return "ErroDesconhecido";
    }

    /**
     * 
     * @param cels
     * @param nomeTabela
     * @param replace
     * @return true/false
     */
    public boolean insereDadosTabela(Cell[][] cels, String nomeTabela, boolean replace) {
        ArrayList<String> pk = null;

        pk = getPrimaryKeysSec(nomeTabela);

        ArrayList<Integer> indexes = new ArrayList<Integer>();
        //int[] indexes;
        //=new int[pk.size()];
        if (pk != null) {
            indexes = getIndexesOfPrimaryKey(pk, nomeTabela);
        } else if (pk == null) {
            replace = false;
        }

        if (replace == true) {

            int nlinhas = devolveNumeroLinhasInserir(cels);

            for (int i = 1; i < nlinhas; i++) {
                ArrayList<String> insert = devolveLinhaInserir(i, cels);
                boolean update = verificaUpdate(pk, indexes, insert, nomeTabela);
                if (update == false) {
                    String singleInsert = devolve1StatementInsert(nomeTabela, insert);
                    try {
                        insertQuery(singleInsert);
                    } catch (Exception e) {
                        logger.log(Level.SEVERE, "Erro ao fazer Insert na Tabela", e);
                        return false;
                    }
                } else if (update == true) {
                    ArrayList<String> colunas = devolveColunasTabela(cels);
                    String singleUpdate = devolveStatementUpdate(nomeTabela, insert, colunas, pk, indexes);
                    try {
                        insertQuery(singleUpdate);
                    } catch (Exception e) {
                        logger.log(Level.SEVERE, "Erro ao fazer Update na Tabela", e);
                        return false;
                    }
                }
            }

        } else if (replace == false) {
            ArrayList<String> statementInsert = criaStatemntInserts(nomeTabela, cels);
            for (int i = 0; i < statementInsert.size(); i++) {
                try {
                    insertQuery(statementInsert.get(i));
                } catch (Exception e) {
                    logger.log(Level.SEVERE, "Erro ao fazer Insert na Tabela", e);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 
     * @param tabela
     * @return ResultSet
     */
    public ResultSet selecionaDadosInseridos(String tabela) {
        ResultSet rs = null;
        try {
            //Seleciona e imprime o que escreveu
            rs = selectQuery("SELECT * FROM " + tabela);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Erro ao fazer Select dos dados inseridos", e);
        }
        return rs;
    }

    //--------------------------------------
    //***METODOS INTERMÉDIOS DA BASE DADOS**
    //--------------------------------------
    /**
     * 
     * @param cel
     * @return 
     */
    ArrayList<String> devolveColunasTabela(Cell[][] cel) {
        //Armazena nomes das colunas na arraylist
        ArrayList<String> colunas = new ArrayList<String>();
        int cont = 0;
        for (Cell[] row : cel) {
            for (Cell cell : row) {
                cont++;
                if (cell.getValue().toString().equals("")) {
                    colunas.add("Column" + cont);
                } else {
                    colunas.add(cell.getValue().toString());
                }
            }
            break;
        }
        //System.out.println("Numero de colunas: "+colunas.size());
        return colunas;
    }

    /**
     * 
     * @param colunas
     * @return 
     */
    private boolean checkCollumnAcceptable(ArrayList<String> colunas) {
        for (int i = 0; i < colunas.size(); i++) {
            try {
                int teste = Integer.parseInt(colunas.get(i).toString());
                return false;
            } catch (Exception exc) {
                //if (colunas.get(i).charAt(0) == '0' || colunas.get(i).charAt(0) == '1' || colunas.get(i).charAt(0) == '2' || colunas.get(i).charAt(0) == '3' || colunas.get(i).charAt(0) == '4' || colunas.get(i).charAt(0) == '5' || colunas.get(i).charAt(0) == '6' || colunas.get(i).charAt(0) == '7' || colunas.get(i).charAt(0) == '8' || colunas.get(i).charAt(0) == '9') {
                if (colunas.get(i).charAt(0) >= '0' && colunas.get(i).charAt(0) <= '9') {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * 
     * @param nomeTabela
     * @return 
     */
    private String criaStatementDropTable(String nomeTabela) {
        String dropTabela = "DROP TABLE " + nomeTabela;
        return dropTabela;
    }

    /**
     * 
     * @param colunas
     * @return 
     */
    private ArrayList<String> mudaNomesColunasSeIgual(ArrayList<String> colunas) {
        for (int j = 0; j < colunas.size(); j++) {
            int teste = j + 1;
            for (int k = teste; k < colunas.size(); k++) {
                if (colunas.get(j).equals(colunas.get(k))) {
                    colunas.set(j, "Column" + j);
                }
            }
        }
        return colunas;
    }

    /**
     * 
     * @param colunas
     * @return 
     */
    private ArrayList<String> mudaNomeColunasSeComecadasPorInt(ArrayList<String> colunas) {
        for (int i = 0; i < colunas.size() - 1; i++) {
            try {
                int teste = Integer.parseInt(colunas.get(i).toString());
                colunas.set(i, "Column" + colunas.get(i).toString());
            } catch (Exception exc) {
                if (colunas.get(i).charAt(0) == '0' || colunas.get(i).charAt(0) == '1' || colunas.get(i).charAt(0) == '2' || colunas.get(i).charAt(0) == '3' || colunas.get(i).charAt(0) == '4' || colunas.get(i).charAt(0) == '5' || colunas.get(i).charAt(0) == '6' || colunas.get(i).charAt(0) == '7' || colunas.get(i).charAt(0) == '8' || colunas.get(i).charAt(0) == '9') {
                    colunas.set(i, "Column" + colunas.get(i).toString());
                }
            }
        }
        return colunas;
    }

    /**
     * 
     * @param nomeTabela
     * @param cel
     * @param colunas
     * @param indexes
     * @return 
     */
    private String criaStatementTabela(String nomeTabela, Cell[][] cel, ArrayList<String> colunas, int[] indexes) {
        //Cria string para criar tabela
        //ArrayList<String> arrayColunas = null;
        String criaTabela = "CREATE TABLE " + nomeTabela + "(";
        int total = 0;

        for (int i = 0; i < colunas.size() - 1; i++) {
            //boolean b = checkCellisPK(i,indexes);
            //if(b==true){
            //    criaTabela=criaTabela+""+colunas.get(i).toString()+"PRIMARY KEY VARCHAR(80), ";
            //}else if(b==false){
            criaTabela = criaTabela + "" + colunas.get(i).toString() + " VARCHAR(80), ";
            //}
            total++;
        }
        //boolean b = checkCellisPK(total,indexes);
        //if(b==true){
        //    criaTabela=criaTabela+""+colunas.get(total).toString()+"PRIMARY KEY VARCHAR(80))";
        //}else if(b==false){
        criaTabela = criaTabela + "" + colunas.get(total).toString() + " VARCHAR(80),";
        //}
        String pk = "PRIMARY KEY(";
        int tam = indexes.length;
        tam--;
        for (int x = 0; x < tam; x++) {
            pk = pk + colunas.get(indexes[x]).toString() + ", ";
        }
        pk = pk + colunas.get(indexes[tam]).toString() + ")";

        //System.out.println(criaTabela);
        //arrayColunas.add(colunas.get(total).toString());
        //System.out.println(criaTabela);

//       String constraint="";
//       
//       if(indexes!=null){
//           int tam = indexes.length;
//           tam--;
//           for(int i=0;i<tam;i++){
//               // CONSTRAINT employees_pk PRIMARY KEY (employee_number)
//               constraint = constraint + "CONSTRAINT " + colunas.get(indexes[i]).toString() +"_pk PRIMARY KEY (" + colunas.get(indexes[i]).toString() + "), ";
//           }
//           //tam++;
//           constraint = constraint + "CONSTRAINT " + colunas.get(indexes[tam]).toString() +"_pk PRIMARY KEY (" + colunas.get(indexes[tam]).toString() + ")";
//       }      
        criaTabela = criaTabela + pk + ")";
        return criaTabela;
    }

    /**
     * 
     * @param ind
     * @param indexes
     * @return 
     */
    private boolean checkCellisPK(int ind, int[] indexes) {
        for (int i = 0; i < indexes.length; i++) {
            if (indexes[i] == ind) {
                return true;
            }
        }
        return false;
    }

    /**
     * 
     * @param nomeTabela
     * @param cel
     * @return 
     */
    private ArrayList<String> criaStatemntInserts(String nomeTabela, Cell[][] cel) {
        //Criar strings para inserir dados na DB
        ArrayList<String> arrayString = new ArrayList<String>();
        ArrayList<String> comandosSQL = new ArrayList<String>();
        int counter = 0;
        for (Cell[] row : cel) {
            if (counter != 0) {
                for (Cell cell : row) {
                    arrayString.add("" + cell.getContent().toString());
                }

                String valores = "";
                int size = arrayString.size() - 1;
                for (int i = 0; i < size; i++) {
                    valores = valores + "'" + arrayString.get(i).toString() + "', ";
                }
                //size++;
                valores = valores + "'" + arrayString.get(size).toString() + "'";
                String insert = "INSERT INTO " + nomeTabela + " VALUES (" + valores + ")";
                comandosSQL.add(insert);
                //System.out.println(insert);
                arrayString = new ArrayList<String>();
            }
            counter++;
        }

        return comandosSQL;
    }

    /**
     * 
     * @param nomeTabela
     * @return 
     */
    private boolean checkNameTableAccepted(String nomeTabela) {
        try {
            int teste = Integer.parseInt(nomeTabela);
            return false;
            //nomeTabela = "Table" + nomeTabela;
            //System.out.println(teste+"");
            //System.out.println(textTabela);
        } catch (Exception exc) {
            if (nomeTabela.charAt(0) >= '0' && nomeTabela.charAt(0) <= '9') {
                //nomeTabela.charAt(0) == '0' || nomeTabela.charAt(0) == '1' || nomeTabela.charAt(0) == '2' || nomeTabela.charAt(0) == '3' || nomeTabela.charAt(0) == '4' || nomeTabela.charAt(0) == '5' || nomeTabela.charAt(0) == '6' || nomeTabela.charAt(0) == '7' || nomeTabela.charAt(0) == '8' || nomeTabela.charAt(0) == '9') {
                return false;
                //nomeTabela = "Table" + nomeTabela;
            }
            return true;
            //Sendo que nao é um INT não fazemos nada
        }
    }

    /**
     * 
     * @param nomeTabela
     * @return 
     */
    private ArrayList<String> getPrimaryKeysSec(String nomeTabela) {
        ArrayList<String> pk = new ArrayList<String>();
        //String st = "select column_name from all_cons_columns acc, all_constraints ac where acc.table_name = ac.table_name and ac.table_name = '" + nomeTabela + "' and ac.constraint_type = 'P'";
        ResultSet rs;
        try {
            //rs = selectQuery(st);
            //ResultSetMetaData meta = rs.getMetaData();
            DatabaseMetaData meta = conn.getMetaData();

            rs = meta.getPrimaryKeys(null, null, nomeTabela);

            while (rs.next()) {
                String columnName = rs.getString("COLUMN_NAME");
                pk.add(columnName);
                //System.out.println("getPrimaryKeys(): columnName=" + columnName);
            }

//            int i=0;
//            Object o = null;
//            for (; rs.next();) {
//                o = rs.getObject(i + 1);    // Is SQL the first column is indexed
//                pk.add(o.toString());
//            }
        } catch (Exception e) {
            pk = null;
        }
        return pk;
    }

    /**
     * 
     * @param nomeTabela
     * @return 
     */
    private ArrayList<String> getPrimaryKeys(String nomeTabela) {
        ArrayList<String> pk = new ArrayList<String>();
        String st = "select column_name from all_cons_columns acc, all_constraints ac where acc.table_name = ac.table_name and ac.table_name = '" + nomeTabela + "' and ac.constraint_type = 'P'";
        ResultSet rs;
        try {
            rs = selectQuery(st);
            ResultSetMetaData meta = rs.getMetaData();
            int i = 0;
            Object o = null;
            for (; rs.next();) {
                o = rs.getObject(i + 1);    // Is SQL the first column is indexed
                pk.add(o.toString());
            }
        } catch (Exception e) {
            pk = null;
        }
        return pk;
    }

    /**
     * 
     * @param rs
     * @return 
     */
    private ArrayList<String> devolveNomeColunasTabelaBD(ResultSet rs) {
        ArrayList<String> retorno = new ArrayList<String>();
        try {
            ResultSetMetaData meta = rs.getMetaData();
            int colmax = meta.getColumnCount();
            for (int k = 1; k <= colmax; k++) {
                retorno.add("" + meta.getColumnName(k));
            }
        } catch (Exception e) {
            retorno = null;
        }
        return retorno;
    }

    /**
     * 
     * @param pk
     * @param tabela
     * @return 
     */
    private ArrayList<Integer> getIndexesOfPrimaryKey(ArrayList<String> pk, String tabela) {
        ArrayList<Integer> index = new ArrayList<Integer>();
        ResultSet rs = null;
        String st = "SELECT * FROM " + tabela;
        try {
            rs = selectQuery(st);
        } catch (Exception e) {
        }
        ArrayList<String> colunas = devolveNomeColunasTabelaBD(rs);

        int counter = 0;
        int sizeCol = colunas.size();
        int sizePK = pk.size();

        for (int i = 0; i < sizeCol; i++) {
            if (counter < sizePK) {
                if (colunas.get(i).toString().equals(pk.get(counter).toString())) {
                    index.add(i);
                    counter++;
                }
            }

        }

        return index;
    }

    /**
     * 
     * @param cels
     * @return 
     */
    private int devolveNumeroLinhasInserir(Cell[][] cels) {
        int nLinhas = 0;

        for (Cell[] row : cels) {
            nLinhas++;
        }

        return nLinhas;
    }

    /**
     * 
     * @param i
     * @param cels
     * @return 
     */
    private ArrayList<String> devolveLinhaInserir(int i, Cell[][] cels) {
        ArrayList<String> conteudoCell = new ArrayList<String>();
        int counter = 0;
        for (Cell[] row : cels) {
            if (counter == i) {
                for (Cell cell : row) {
                    conteudoCell.add("" + cell.getContent().toString());
                }
            }
            counter++;
        }

        return conteudoCell;
    }

    /**
     * 
     * @param pk
     * @param indexes
     * @param insert
     * @param tabela
     * @return 
     */
    private boolean verificaUpdate(ArrayList<String> pk, ArrayList<Integer> indexes, ArrayList<String> insert, String tabela) {
        boolean b = false;
        String st = "";

        String tempWhere = "";
        int size2 = pk.size() - 1;
        for (int j = 0; j < size2; j++) {
            tempWhere = tempWhere + "" + pk.get(j).toString() + "=" + insert.get(indexes.get(j)) + "AND ";
        }
        tempWhere = tempWhere + "" + pk.get(size2).toString() + "=" + insert.get(indexes.get(size2));

        st = "SELECT * FROM " + tabela + " WHERE " + tempWhere;
        int linhas = 0;
        try {
            ResultSet rs = selectQuery(st);
            ResultSetMetaData meta = rs.getMetaData();
            printQuery(rs);
            linhas = 1;
//            for (; rs.next();) {
//                linhas++;
//            }
        } catch (Exception e) {
            linhas = 0;
        }

        if (linhas == 0) {
            b = false;
        } else if (linhas != 0) {
            b = true;
        }

        return b;
    }

    /**
     * 
     * @param nomeTabela
     * @param insert
     * @return 
     */
    private String devolve1StatementInsert(String nomeTabela, ArrayList<String> insert) {
        String st;
        String temp = "";
        int size = insert.size() - 1;
        for (int i = 0; i < size; i++) {
            temp = temp + "'" + insert.get(i).toString() + "', ";
        }
        temp = temp + "'" + insert.get(size).toString() + "'";
        st = "INSERT INTO " + nomeTabela + " VALUES(" + temp + ")";
        return st;
    }

    /**
     * 
     * @param nomeTabela
     * @param insert
     * @param colunas
     * @param pk
     * @param indexes
     * @return 
     */
    private String devolveStatementUpdate(String nomeTabela, ArrayList<String> insert, ArrayList<String> colunas, ArrayList<String> pk, ArrayList<Integer> indexes) {
        String st = "";
        String temp = "";
        int size = colunas.size() - 1;
        for (int i = 0; i < size; i++) {
            temp = temp + "" + colunas.get(i).toString() + "=" + insert.get(i).toString() + ",";
        }
        temp = temp + colunas.get(size).toString() + "=" + insert.get(size).toString();

        String tempWhere = "";
        int size2 = pk.size() - 1;
        for (int j = 0; j < size2; j++) {
            tempWhere = tempWhere + "" + pk.get(j).toString() + "=" + insert.get(indexes.get(j)) + "AND ";
        }
        tempWhere = tempWhere + "" + pk.get(size2).toString() + "=" + insert.get(indexes.get(size2));

        st = "UPDATE " + nomeTabela + " SET " + temp + " WHERE " + tempWhere;

        return st;
    }

    //--------------------------------------
    //***METODOS EXCLUSIVOS DA BASE DADOS***
    //--------------------------------------
    /**
     * 
     * @param dbName
     * @param dbUser
     * @param dbPassword
     * @throws Exception 
     */
    private void turnDBOn(String dbName, String dbUser, String dbPassword) throws Exception {
        // Load the HSQL Database Engine JDBC driver
        // hsqldb.jar should be in the class path or made part of the current jar
        Class.forName("org.hsqldb.jdbcDriver");
        conn = DriverManager.getConnection("jdbc:hsqldb:file:" + dbName, dbUser, dbPassword);
    }

    //Usado para comandos SELECT
    /**
     * 
     * @param expression
     * @return
     * @throws SQLException 
     */
    private synchronized ResultSet selectQuery(String expression) throws SQLException {
        ResultSet rs = null;
        stmt = conn.createStatement();         // statement objects can be reused with
        // repeated calls to execute but we
        // choose to make a new one each time
        rs = stmt.executeQuery(expression);    // run the query
        // do something with the result set.
        //printQuery(rs);
        //st.close();    // NOTE!! if you close a statement the associated ResultSet is
        return rs;     // closed too
        // so you should copy the contents to some other object.
        // the result set is invalidated also  if you recycle an Statement
        // and try to execute some other query before the result set has been
        // completely examined.
    }

    //use for SQL commands CREATE, DROP, INSERT and UPDATE
    /**
     * 
     * @param expression
     * @return
     * @throws SQLException 
     */
    private synchronized boolean insertQuery(String expression) throws SQLException {

        stmt = conn.createStatement();    // statements

        int i = stmt.executeUpdate(expression);    // run the query

        if (i == -1) {
            return false;
            //System.out.println("db error : " + expression);
        }

        //st.close();
        return true;
    }

    /**
     * 
     * @param tableName
     * @return 
     */
    public boolean checkTableExistence(String tableName) {

        String queryString = "SELECT top 1 * from " + tableName;
        boolean result = false;

        try {
            ResultSet rs = selectQuery(queryString);
            result = true;
        } catch (Exception ex) {
            result = false;
        }
        return result;
    }

    /**
     * 
     * @param rs
     * @throws SQLException 
     */
    private static void printQuery(ResultSet rs) throws SQLException {

        // the order of the rows in a cursor
        // are implementation dependent unless you use the SQL ORDER statement
        ResultSetMetaData meta = rs.getMetaData();
        int colmax = meta.getColumnCount();
        int i;
        Object o = null;

        // the result set is a cursor into the data.  You can only
        // point to one row at a time
        // assume we are pointing to BEFORE the first row
        // rs.next() points to next row and returns true
        // or false if there is no next row, which breaks the loop
        for (; rs.next();) {
            for (i = 0; i < colmax; ++i) {
                o = rs.getObject(i + 1);    // Is SQL the first column is indexed

                // with 1 not 0
                System.out.print(o.toString() + " ");
            }
            System.out.println(" ");
        }
    }

    /**
     * 
     * @throws SQLException 
     */
    private void turnDBOff() throws SQLException {
        //Statement st = conn.createStatement();
        // db writes out to files and performs clean shuts down
        // otherwise there will be an unclean shutdown
        // when program ends
        stmt.execute("SHUTDOWN");

        if (stmt != null) {
            stmt.close();
        }

        this.conn.close();    // if there are no other open connection
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // metodos para base de dados de sincronização   
    public String criarTabelaSync(String tabela) {

        boolean tableExists = checkTableExistence(tabela);
        if (tableExists == true) {
            return "NomeTabelaExiste";
        }

        boolean tableAccept = checkNameTableAccepted(tabela);
        if (tableAccept == false) {
            return "NomeTabelaInvalido";
        }

        String createTable = criaStatementTabelaSync(tabela);

        try {
            insertQuery(createTable);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Erro ao fazer Create Table", e);
            return "ErroDesconhecido";
        }

        return "TabelaCriada";
    }

    private String criaStatementTabelaSync(String nomeTabela) {
        //estritura da tabela irá ter linha . coluna . valor da celula.
        String criaTabela = "CREATE TABLE " + nomeTabela + "(linha integer, coluna integer, dados varchar(80))";

        return criaTabela;
    }


    public boolean insereDadosTabelaSync(Cell[][] cels, String nomeTabela) {
        ArrayList<String> statementInsert = criaStatemntInsertsSync(nomeTabela, cels);
        for (int i = 0; i < statementInsert.size(); i++) {
            try {
                insertQuery(statementInsert.get(i));
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Erro ao fazer Insert na Tabela", e);
                return false;
            }
        }

        return true;
    }

    private ArrayList<String> criaStatemntInsertsSync(String nomeTabela, Cell[][] cel) {
        //Criar strings para inserir dados na DB

        ArrayList<String> comandosSQL = new ArrayList<String>();

        for (Cell[] row : cel) {

            for (Cell cell : row) {
                int col = cell.getAddress().getColumn();
                int line = cell.getAddress().getRow();
                String content = cell.getContent().toString();
                comandosSQL.add("INSERT INTO " + nomeTabela + " VALUES(" + col + " , " + line + " , '" + content + "')");
            } // salta de linha

        }

        return comandosSQL;
    }
}
