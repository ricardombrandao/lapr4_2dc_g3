/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db;

import csheets.core.Cell;
import csheets.ext.db.ui.PainelMessage;
import csheets.ui.ctrl.UIController;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

/**
 *
 * @author Luis Mendes
 */
public class SynchronizeHSQLDBLogic {

    private UIController uiController;
    private Cell[][] celulas;
    private String nomeTabela;
    private String BD;
    Connection conn;
    private int rowMax;
    private int columnMax;
    private String option;

    public SynchronizeHSQLDBLogic() {
    }

    public String[][] syncHSQLDB(String nome, UIController ui, String[][] matrixAnt, String opt) {
        option = opt;
        uiController = ui;
        //celulas = cel;
        nomeTabela = nome;
        BD = "syncfiles\\SyncHSQLDB";
        System.out.println("Classe de Sync HSQLDB!");
        String matrixDataBase[][] = null;
        String matrixWorkbook[][] = null;
        String matrixSync[][] = null;
        columnMax = 3;
        //////////////////////////////////////////////////////////////////////////////////////
        //ligarDB
        try {
            Class.forName("org.hsqldb.jdbcDriver");
            conn = DriverManager.getConnection("jdbc:hsqldb:file:" + BD, "sa", "");

            //System.out.println(nomeTabela);
            Statement st = null;
            st = conn.createStatement();

            ResultSet rsa = st.executeQuery("SELECT COUNT(*) FROM " + nomeTabela);
            ResultSetMetaData meta2 = rsa.getMetaData();
            rsa.next();
            int nLines = Integer.parseInt(rsa.getObject(1).toString());
            matrixDataBase = new String[nLines][3];
            matrixWorkbook = new String[nLines][3];
            rowMax = nLines;


            ResultSet rs = st.executeQuery("SELECT * FROM " + nomeTabela);
            ResultSetMetaData meta = rs.getMetaData();

            int colMax = meta.getColumnCount();
            //System.out.println("colunas: " + colMax);
            int Column = 0, Row = 0;
            Object o = null;
            for (; rs.next();) {
                for (int i = 0; i < colMax; i++) {
                    o = rs.getObject(i + 1);
                    matrixDataBase[Row][Column] = o.toString();

                    Column++;
                }
                System.out.println("Valores base de dados: " + matrixDataBase[Row][0] + "," + matrixDataBase[Row][1] + "," + matrixDataBase[Row][2]);
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //// valores do workbook
                String valueWB = uiController.getActiveSpreadsheet().getCell(Integer.parseInt(matrixDataBase[Row][0]), Integer.parseInt(matrixDataBase[Row][1])).getContent();
                //System.out.println("valor Work: = " + valueWB);
                matrixWorkbook[Row][0] = matrixDataBase[Row][0];
                matrixWorkbook[Row][1] = matrixDataBase[Row][1];
                matrixWorkbook[Row][2] = valueWB;
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                Row++;
                Column = 0;
            }
            st.execute("SHUTDOWN");
            conn.close();
        } catch (Exception ex) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Erro na sincronização!!");
            System.out.println("---------------------------------------- Erro no sync HSQLDB --------------------------------------------------");
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // verifica se a matriz sinc esta a null... ou seja nunca houve sync
        if (matrixAnt == null) {
            //matrixSync = matrixDataBase;
            matrixSync = matrixWorkbook;


        } else {
            matrixSync = matrixAnt;
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // comparações
        try {
            for (int i = 0; i < rowMax; i++) {
                for (int j = 2; j < columnMax; j++) {
                    if ((matrixWorkbook[i][j]).equals(matrixDataBase[i][j])) {
                        System.out.println("Sem alterações, Não faz nada");
                    } else if (!(matrixWorkbook[i][j]).equals(matrixDataBase[i][j]) && !(matrixWorkbook[i][j]).equals(matrixSync[i][j]) && !(matrixDataBase[i][j]).equals(matrixSync[i][j])) {
                        System.out.println("Fazer merge, Diferente em todos os lados");
                        if (option.equals("WorkBook")) {
                            System.out.println("Preferencia ao Workbook");
                            //fazer select
                            Class.forName("org.hsqldb.jdbcDriver");
                            conn = DriverManager.getConnection("jdbc:hsqldb:file:" + BD, "sa", "");
                            Statement st = null;
                            st = conn.createStatement();
                            ResultSet rs = st.executeQuery("UPDATE " + nomeTabela + " SET DADOS='" + matrixWorkbook[i][2] + "' WHERE LINHA=" + matrixWorkbook[i][0] + " AND COLUNA=" + matrixWorkbook[i][1]);
                            //ResultSetMetaData meta = rs.getMetaData();
                            conn.commit();

                            st.execute("SHUTDOWN");
                            conn.close();
                            break;

                        } else {
                            System.out.println("Preferencia a base de dados");
                            uiController.getActiveSpreadsheet().getCell(Integer.parseInt(matrixDataBase[i][0]), Integer.parseInt(matrixDataBase[i][1])).setContent(matrixDataBase[i][2]);
                            matrixWorkbook[i][0] = matrixDataBase[i][0];
                            matrixWorkbook[i][1] = matrixDataBase[i][1];
                            matrixWorkbook[i][2] = matrixDataBase[i][2];
                            break;
                        }


                    } else if (!(matrixWorkbook[i][j]).equals(matrixDataBase[i][j])) {
                        System.out.println("ver na sync temporaria");
                        if (!(matrixWorkbook[i][j]).equals(matrixSync[i][j])) {

                            System.out.println("Valor actualizado no workbbok");
                            Class.forName("org.hsqldb.jdbcDriver");
                            conn = DriverManager.getConnection("jdbc:hsqldb:file:" + BD, "sa", "");
                            Statement st = null;
                            st = conn.createStatement();
                            ResultSet rs = st.executeQuery("UPDATE " + nomeTabela + " SET DADOS='" + matrixWorkbook[i][2] + "' WHERE LINHA=" + matrixWorkbook[i][0] + " AND COLUNA=" + matrixWorkbook[i][1]);
                            //ResultSetMetaData meta = rs.getMetaData();
                            conn.commit();

                            st.execute("SHUTDOWN");
                            conn.close();
                            break;

                        } else if (!(matrixDataBase[i][j]).equals(matrixSync[i][j])) {
                            System.out.println("Valor actualizado na base de dados");
                            uiController.getActiveSpreadsheet().getCell(Integer.parseInt(matrixDataBase[i][0]), Integer.parseInt(matrixDataBase[i][1])).setContent(matrixDataBase[i][2]);
                            matrixWorkbook[i][0] = matrixDataBase[i][0];
                            matrixWorkbook[i][1] = matrixDataBase[i][1];
                            matrixWorkbook[i][2] = matrixDataBase[i][2];
                            break;
                        } else {
                            System.out.println("######################################################################################################");
                        }
                    }

                }
            }
        } catch (Exception ex) {
            System.out.println("Erro na sincronização!!");
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Erro na sincronização!!");
        }
        matrixSync = matrixWorkbook;
        return matrixSync;


    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
