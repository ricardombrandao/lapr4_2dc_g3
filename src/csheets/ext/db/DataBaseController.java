/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db;

import csheets.core.Cell;
import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.ext.db.ui.DataBaseImportPane;
import csheets.ext.db.ui.DataBaseInfoPane;
import csheets.ext.db.ui.DataBaseShowResults;
import csheets.ext.db.ui.PainelMessage;
import csheets.ui.ctrl.UIController;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Esta classe vai instanciar a factory da Base de dados e tratar de tudo para gerar a base de dados atraves dos dados escolhidos pelo utilizador
 */
public class DataBaseController {

    public  DataBaseController(){}
    
    /**
     * 
     * @param driverDB
     * @param celulas
     * @param textNomeDB
     * @param textUsername
     * @param textPassword
     * @param textTabela
     * @param ui
     * @param indexes 
     */
    public void DataBaseExport(String driverDB, Cell[][] celulas,String textNomeDB,String textUsername,String textPassword,String textTabela,UIController ui, int[] indexes){
        //System.out.println("Base de dados driver: "+driverDB);
        //System.out.println("Base de dados: "+textNomeDB);
        //System.out.println("Username: "+textUsername);
        //System.out.println("Password: "+textPassword);
        //System.out.println("Tabela:"+textTabela);
        
        DataBaseFactory bd = new DataBaseFactory(driverDB);
        try{
            DataBaseUtilHSQLDB driverHSQLDB = (DataBaseUtilHSQLDB)bd.getObjectDriver();
            this.RegistarBDHSQLDB(driverHSQLDB,celulas, textNomeDB, textUsername, textPassword, textTabela, ui, indexes);
        }catch(Exception ex){
             //2º parte do programa entra aqui
            try{
            DataBaseUtilDERBY driverDERBY = (DataBaseUtilDERBY)bd.getObjectDriver();
            this.RegistarBDDERBY(driverDERBY, celulas, textNomeDB, textUsername, textPassword, textTabela, ui, indexes);
            } catch(Exception e){
                //Novas bases de dados devem aparecer aqui
            }
        }
    }
    
    /**
     * 
     * @param driverHSQLDB
     * @param celulas
     * @param textNomeDB
     * @param textUsername
     * @param textPassword
     * @param textTabela
     * @param ui
     * @param indexes
     * @throws Exception 
     */
    private void RegistarBDHSQLDB(DataBaseUtilHSQLDB driverHSQLDB, Cell[][] celulas, String textNomeDB, String textUsername, String textPassword, String textTabela, UIController ui, int[] indexes) throws Exception {


        //Ligar á base de dados, se ela não existir o driver cria automaticamente uma com o nome escolhido
        boolean baseDadosOn = driverHSQLDB.ligarBaseDados(textNomeDB, textUsername, textPassword);
        //System.out.println("DataBase ON");
        if (baseDadosOn == false) {
            PainelMessage mensagemDBOn = new PainelMessage();
            mensagemDBOn.PainelOk("Dados de login na Base de Dados errados");
            DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
            return;
            //JOptionPane.showMessageDialog(null, "Dados de login na base de dados errados");
        }

        //Manda criar a tabela
        String resultadoCriarTabela = driverHSQLDB.criarTabela(textTabela, celulas, false,indexes);
        String resultadoCriarTabela2 = "";
        boolean escreverCima=true;
        boolean update=false;
        if (resultadoCriarTabela.equals("NomeTabelaInvalido")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Nome da Tabela escolhido é inválido");
            DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
            driverHSQLDB.desligarBaseDados();
            return;
        } else if (resultadoCriarTabela.equals("NomeColunaInvalido")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Nome de uma ou mais colunas escolhido é inválido");
            DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
            driverHSQLDB.desligarBaseDados();
            return;
        } else if (resultadoCriarTabela.equals("ErroDesconhecido") || resultadoCriarTabela.equals("")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Erro Desconhecido");
            DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
            driverHSQLDB.desligarBaseDados();
            return;
        } else if (resultadoCriarTabela.equals("NomeTabelaExiste")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            escreverCima = mensagemErroTabela.PainelYesNo("A tabela já existe, deseja escrever por cima?");
            if (escreverCima == true) {
                String secundario = driverHSQLDB.criarTabela(textTabela, celulas, true, indexes);
                resultadoCriarTabela2 = secundario;}
            else if(escreverCima==false){
                PainelMessage updateYesNo = new PainelMessage();
                update = updateYesNo.PainelYesNo("Se existirem dados repetidos deseja fazer update?");
                resultadoCriarTabela2="TabelaCriada";
            }
            //} else if (escreverCima == false) {
            //    driverHSQLDB.desligarBaseDados();
            //    DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
            //    return;
        } //System.out.println("Codigo create table executado");
        if (resultadoCriarTabela.equals("TabelaCriada") || resultadoCriarTabela2.equals("TabelaCriada")) {
                boolean dadosInseridos = driverHSQLDB.insereDadosTabela(celulas, textTabela,update);
                if (dadosInseridos == false) {
                    PainelMessage mensagemErroTabela = new PainelMessage();
                    mensagemErroTabela.PainelOk("Erro a inserir dados na base dados");
                    DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
                    driverHSQLDB.desligarBaseDados();
                    return;
                }
                ResultSet rs = driverHSQLDB.selecionaDadosInseridos(textTabela);
                if (rs == null) {
                    PainelMessage mensagemErroTabela = new PainelMessage();
                    mensagemErroTabela.PainelOk("Erro a selecionar dados inseridos na base dados");
                    DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
                    driverHSQLDB.desligarBaseDados();
                    return;
                }
                ArrayList<String> colunas = driverHSQLDB.devolveColunasTabela(celulas);
                DataBaseShowResults mostraResultado = new DataBaseShowResults(rs, colunas);
                driverHSQLDB.desligarBaseDados();
            }
         
    }

    /**
     * 
     * @param driverDB
     * @param cel
     * @param textNomeDB
     * @param textUsername
     * @param textPassword
     * @param textTabela
     * @param ui 
     */
    public void DataBaseImport(String driverDB, Cell cel,String textNomeDB,String textUsername,String textPassword,String textTabela,UIController ui){
        DataBaseFactory bd = new DataBaseFactory(driverDB);
        try{
            DataBaseUtilHSQLDB driverHSQLDB = (DataBaseUtilHSQLDB)bd.getObjectDriver();
            this.importarBDHQSLDB(driverHSQLDB,cel, textNomeDB, textUsername, textPassword, textTabela, ui);
        }catch(Exception ex){
             //2º parte do programa entra aqui
            DataBaseUtilDERBY driverDERBY = (DataBaseUtilDERBY)bd.getObjectDriver();
            this.importarBDDERBY(driverDERBY, cel, textNomeDB, textUsername, textPassword, textTabela, ui);
        }
    }
    
    /**
     * 
     * @param driverHSQLDB
     * @param cel
     * @param textNomeDB
     * @param textUsername
     * @param textPassword
     * @param textTabela
     * @param ui 
     */
    private void importarBDHQSLDB(DataBaseUtilHSQLDB driverHSQLDB, Cell cel, String textNomeDB, String textUsername, String textPassword, String textTabela, UIController ui) {
        //Ligar á base de dados, se ela não existir o driver cria automaticamente uma com o nome escolhido
        boolean baseDadosOn = driverHSQLDB.ligarBaseDados(textNomeDB, textUsername, textPassword);
        //System.out.println("DataBase ON");
        if (baseDadosOn == false) {
            PainelMessage mensagemDBOn = new PainelMessage();
            mensagemDBOn.PainelOk("Dados de login na Base de Dados errados");
            DataBaseImportPane infoPanel = new DataBaseImportPane(cel, ui, textNomeDB, textUsername, textPassword, textTabela);
            return;
            //JOptionPane.showMessageDialog(null, "Dados de login na base de dados errados");
        }

        boolean tabelaExtists = driverHSQLDB. checkTableExistence(textTabela);
        if(tabelaExtists==false)
        {
            PainelMessage mensagemDBOn = new PainelMessage();
            mensagemDBOn.PainelOk("A tabela escolhida não existe");
            DataBaseImportPane infoPanel = new DataBaseImportPane(cel, ui, textNomeDB, textUsername, textPassword, textTabela);
            return;
        }
        
        ResultSet rs = driverHSQLDB.selecionaDadosInseridos(textTabela);
        if (rs == null) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Erro a selecionar dados inseridos na base dados");
            //DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
            driverHSQLDB.desligarBaseDados();
            return;
        }
        
        int coluna=cel.getAddress().getColumn();
        int linha=cel.getAddress().getRow();
                
        String conteudoCelula;
        
        ResultSetMetaData meta = null;
        int colmax;
        int i;
        Object o = null;
        try {
            meta = rs.getMetaData();
            colmax = meta.getColumnCount();
            for (int k = 1; k <= colmax; k++)
            {
                try {
                  ui.getActiveSpreadsheet().getCell(coluna, linha).setContent(meta.getColumnName(k));
                } catch (FormulaCompilationException ex) {
                  Logger.getLogger(DataBaseController.class.getName()).log(Level.SEVERE, null, ex);
                }
                coluna++;
            }
            
            linha++;
            coluna=0;
            
            for (; rs.next();) {
                for (i = 0; i < colmax; ++i) {
                    o = rs.getObject(i + 1);
                    conteudoCelula = o.toString() + "";
                    try {
                        ui.getActiveSpreadsheet().getCell(coluna, linha).setContent(conteudoCelula);
                    } catch (FormulaCompilationException ex) {
                        Logger.getLogger(DataBaseController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    coluna++;
                }
                linha++;
                coluna=0;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        PainelMessage mensagemImport = new PainelMessage();
        mensagemImport.PainelOk("Dados importados com sucesso");
        
        driverHSQLDB.desligarBaseDados();
    }

    /**
     * 
     * @param driverDERBY
     * @param celulas
     * @param textNomeDB
     * @param textUsername
     * @param textPassword
     * @param textTabela
     * @param ui
     * @param indexes
     * @throws Exception 
     */
    private void RegistarBDDERBY(DataBaseUtilDERBY driverDERBY, Cell[][] celulas, String textNomeDB, String textUsername, String textPassword, String textTabela, UIController ui, int[] indexes) throws Exception {
//Ligar á base de dados, se ela não existir o driver cria automaticamente uma com o nome escolhido
        boolean baseDadosOn = driverDERBY.ligarBaseDados(textNomeDB, textUsername, textPassword);
        //System.out.println("DataBase ON");
        if (baseDadosOn == false) {
            PainelMessage mensagemDBOn = new PainelMessage();
            mensagemDBOn.PainelOk("Dados de login na Base de Dados errados");
            DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
            return;
            //JOptionPane.showMessageDialog(null, "Dados de login na base de dados errados");
        }

        //Manda criar a tabela
        String resultadoCriarTabela = driverDERBY.criarTabela(textTabela, celulas, false,indexes);
        String resultadoCriarTabela2 = "";
        boolean escreverCima=true;
        boolean update=false;
        if (resultadoCriarTabela.equals("NomeTabelaInvalido")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Nome da Tabela escolhido é inválido");
            DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
            driverDERBY.desligarBaseDados(textNomeDB, textUsername, textPassword);
            return;
        } else if (resultadoCriarTabela.equals("NomeColunaInvalido")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Nome de uma ou mais colunas escolhido é inválido");
            DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
            driverDERBY.desligarBaseDados(textNomeDB, textUsername, textPassword);
            return;
        } else if (resultadoCriarTabela.equals("ErroDesconhecido") || resultadoCriarTabela.equals("")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Erro Desconhecido");
            DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
            driverDERBY.desligarBaseDados(textNomeDB, textUsername, textPassword);
            return;
        } else if (resultadoCriarTabela.equals("NomeTabelaExiste")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            escreverCima = mensagemErroTabela.PainelYesNo("A tabela já existe, deseja escrever por cima?");
            if (escreverCima == true) {
                String secundario = driverDERBY.criarTabela(textTabela, celulas, true, indexes);
                resultadoCriarTabela2 = secundario;}
            else if(escreverCima==false){
                PainelMessage updateYesNo = new PainelMessage();
                update = updateYesNo.PainelYesNo("Se existirem dados repetidos deseja fazer update?");
                resultadoCriarTabela2="TabelaCriada";
            }
            //} else if (escreverCima == false) {
            //    driverHSQLDB.desligarBaseDados();
            //    DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
            //    return;
        } //System.out.println("Codigo create table executado");
        if (resultadoCriarTabela.equals("TabelaCriada") || resultadoCriarTabela2.equals("TabelaCriada")) {
                boolean dadosInseridos = driverDERBY.insereDadosTabela(celulas, textTabela,update);
                if (dadosInseridos == false) {
                    PainelMessage mensagemErroTabela = new PainelMessage();
                    mensagemErroTabela.PainelOk("Erro a inserir dados na base dados");
                    DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
                    driverDERBY.desligarBaseDados(textNomeDB, textUsername, textPassword);
                    return;
                }
                ResultSet rs = driverDERBY.selecionaDadosInseridos(textTabela);
                if (rs == null) {
                    PainelMessage mensagemErroTabela = new PainelMessage();
                    mensagemErroTabela.PainelOk("Erro a selecionar dados inseridos na base dados");
                    DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
                    driverDERBY.desligarBaseDados(textNomeDB, textUsername, textPassword);
                    return;
                }
                ArrayList<String> colunas = driverDERBY.devolveColunasTabela(celulas);
                DataBaseShowResults mostraResultado = new DataBaseShowResults(rs, colunas);
                driverDERBY.desligarBaseDados(textNomeDB, textUsername, textPassword);
            }
         
    }

    /**
     * 
     * @param driverDERBY
     * @param cel
     * @param textNomeDB
     * @param textUsername
     * @param textPassword
     * @param textTabela
     * @param ui 
     */
    private void importarBDDERBY(DataBaseUtilDERBY driverDERBY, Cell cel, String textNomeDB, String textUsername, String textPassword, String textTabela, UIController ui) {
        //Ligar á base de dados, se ela não existir o driver cria automaticamente uma com o nome escolhido
        boolean baseDadosOn = driverDERBY.ligarBaseDadosConsulta(textNomeDB, textUsername, textPassword);
        //System.out.println("DataBase ON");
        if (baseDadosOn == false) {
            PainelMessage mensagemDBOn = new PainelMessage();
            mensagemDBOn.PainelOk("Dados de login na Base de Dados errados");
            DataBaseImportPane infoPanel = new DataBaseImportPane(cel, ui, textNomeDB, textUsername, textPassword, textTabela);
            return;
            //JOptionPane.showMessageDialog(null, "Dados de login na base de dados errados");
        }

        boolean tabelaExtists = driverDERBY. checkTableExistence(textTabela);
        if(tabelaExtists==false)
        {
            PainelMessage mensagemDBOn = new PainelMessage();
            mensagemDBOn.PainelOk("A tabela escolhida não existe");
            DataBaseImportPane infoPanel = new DataBaseImportPane(cel, ui, textNomeDB, textUsername, textPassword, textTabela);
            return;
        }
        
        ResultSet rs = driverDERBY.selecionaDadosInseridos(textTabela);
        if (rs == null) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Erro a selecionar dados da base dados");
            //DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
            driverDERBY.desligarBaseDados(textNomeDB, textUsername, textPassword);
            return;
        }
        
        int coluna=cel.getAddress().getColumn();
        int linha=cel.getAddress().getRow();
                
        String conteudoCelula;
        
        ResultSetMetaData meta = null;
        int colmax;
        int i;
        Object o = null;
        try {
            meta = rs.getMetaData();
            colmax = meta.getColumnCount();
            for (int k = 1; k <= colmax; k++)
            {
                try {
                  ui.getActiveSpreadsheet().getCell(coluna, linha).setContent(meta.getColumnName(k));
                } catch (FormulaCompilationException ex) {
                  Logger.getLogger(DataBaseController.class.getName()).log(Level.SEVERE, null, ex);
                }
                coluna++;
            }
            
            linha++;
            coluna=0;
            
            for (; rs.next();) {
                for (i = 0; i < colmax; ++i) {
                    o = rs.getObject(i + 1);
                    conteudoCelula = o.toString() + "";
                    try {
                        ui.getActiveSpreadsheet().getCell(coluna, linha).setContent(conteudoCelula);
                    } catch (FormulaCompilationException ex) {
                        Logger.getLogger(DataBaseController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    coluna++;
                }
                linha++;
                coluna=0;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        PainelMessage mensagemImport = new PainelMessage();
        mensagemImport.PainelOk("Dados importados com sucesso");
        
        driverDERBY.desligarBaseDados(textNomeDB, textUsername, textPassword);
    }   

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //para a criação os metodos sincronização
    private void RegistarSyncDERBY(DataBaseUtilDERBY driverDERBY, Cell[][] celulas, String nomeTabela) {
        //Ligar á base de dados, se ela não existir o driver cria automaticamente uma com o nome escolhido
        boolean baseDadosOn = driverDERBY.ligarBaseDados("Sync2", "SA", "");
        //System.out.println("DataBase ON");
        if (baseDadosOn == false) {
            PainelMessage mensagemDBOn = new PainelMessage();
            mensagemDBOn.PainelOk("Dados de login na Base de Dados errados");
            return;
        }

        //Manda criar a tabela
        String resultadoCriarTabela = driverDERBY.criarTabelaSync(nomeTabela);
        if (resultadoCriarTabela.equals("NomeTabelaInvalido")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Nome da Tabela escolhido é inválido");
            // se ecistir da erro
            
            driverDERBY.desligarBaseDados("Sync2", "SA", "");
            return;
        } else if (resultadoCriarTabela.equals("ErroDesconhecido") || resultadoCriarTabela.equals("")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Erro Desconhecido");
            //DataBaseInfoPane infoPanel = new DataBaseInfoPane(celulas, ui, textNomeDB, textUsername, textPassword, textTabela);
            driverDERBY.desligarBaseDados("Sync2", "SA", "");
            return;
        } else if (resultadoCriarTabela.equals("NomeTabelaExiste")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            System.out.println("Tabela j+a existe");
            mensagemErroTabela.PainelOk("Tabela já existe, escolha outro nome");  
        } 
        if (resultadoCriarTabela.equals("TabelaCriada")) {
                // é false para não fazer updates na tabela
                boolean dadosInseridos = driverDERBY.insereDadosTabelaSync(celulas, nomeTabela);
                if (dadosInseridos == false) {
                    PainelMessage mensagemErroTabela = new PainelMessage();
                    mensagemErroTabela.PainelOk("Erro a inserir dados na base dados");
                    driverDERBY.desligarBaseDados("Sync2", "SA", "");
                    return;
                }
                driverDERBY.desligarBaseDados("Sync2", "SA", "");
            }
         
    }
    
        public void DataBaseSyncCreator(String nomeTabela, String type, Cell [][] celulas){       
        DataBaseFactory bd = new DataBaseFactory(type);
        // como so existem 2 faze-mos com try catch. No caso de existirem mais poderia ser feito um instanceOf pelo tipo de driver
        try{
            DataBaseUtilHSQLDB driverHSQLDB = (DataBaseUtilHSQLDB)bd.getObjectDriver();
            //this.importarBDHQSLDB(driverHSQLDB,cel, textNomeDB, textUsername, textPassword, textTabela, ui);
            this.RegistarSyncBDHSQLDB(driverHSQLDB, celulas, nomeTabela );
        }catch(Exception ex){
             //2º parte do programa entra aqui
            DataBaseUtilDERBY driverDERBY = (DataBaseUtilDERBY)bd.getObjectDriver();
            this.RegistarSyncDERBY(driverDERBY, celulas, nomeTabela);
        }
    }
     
      private void RegistarSyncBDHSQLDB(DataBaseUtilHSQLDB driverHSQLDB, Cell[][] celulas, String nomeTabela) throws Exception {
        //Ligar á base de dados, se ela não existir o driver cria automaticamente uma com o nome escolhido
        boolean baseDadosOn = driverHSQLDB.ligarBaseDados("syncfiles\\SyncHSQLDB", "SA", "");
        //System.out.println("DataBase ON");
        if (baseDadosOn == false) {
            PainelMessage mensagemDBOn = new PainelMessage();
            mensagemDBOn.PainelOk("Dados de login na Base de Dados errados");
            return;
        }

        //Manda criar a tabela
        String resultadoCriarTabela = driverHSQLDB.criarTabelaSync(nomeTabela);
        if (resultadoCriarTabela.equals("NomeTabelaInvalido")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Nome da Tabela escolhido é inválido");
            // se ecistir da erro
            
            driverHSQLDB.desligarBaseDados();
            return;
        } else if (resultadoCriarTabela.equals("ErroDesconhecido") || resultadoCriarTabela.equals("")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            mensagemErroTabela.PainelOk("Erro Desconhecido");
            driverHSQLDB.desligarBaseDados();
            return;
        } else if (resultadoCriarTabela.equals("NomeTabelaExiste")) {
            PainelMessage mensagemErroTabela = new PainelMessage();
            System.out.println("Tabela j+a existe");
            mensagemErroTabela.PainelOk("Tabela já existe, escolha outro nome");
        } 
        if (resultadoCriarTabela.equals("TabelaCriada")) {
                // é false para não fazer updates na tabela
                boolean dadosInseridos = driverHSQLDB.insereDadosTabelaSync(celulas, nomeTabela);
                if (dadosInseridos == false) {
                    PainelMessage mensagemErroTabela = new PainelMessage();
                    mensagemErroTabela.PainelOk("Erro a inserir dados na base dados");
                    driverHSQLDB.desligarBaseDados();
                    return;
                }
                driverHSQLDB.desligarBaseDados();
            }
         
    }
    
    
     
}