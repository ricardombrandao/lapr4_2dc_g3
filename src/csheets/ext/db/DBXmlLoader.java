/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db;

import csheets.CleanSheets;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;

/**
 *
 * @author Scare
 */
public class DBXmlLoader {

    private static DBXmlLoader instance = new DBXmlLoader();
    private ArrayList<String> drivers;

    private DBXmlLoader() {
        drivers = new ArrayList<String>();
    }

    public static DBXmlLoader getInstance() {
        return instance;
    }

    public ArrayList<String> getDBList() throws Exception {
        loadList();
        return drivers;
    }

    public void loadList() throws Exception {
        drivers.removeAll(drivers);
        
	//InputStream is = ExtensionDataBase.class.getResourceAsStream("res/dbinfo.xml");
        
        URL url = new URL(CleanSheets.class.getResource("res/dbinfo.xml").toString());
        String path = URLDecoder.decode(url.getFile(), "UTF-8");
        File fXmlFile = new File(path);
        
        
        
        
        //File fXmlFile = new File("src/csheets/export_import/config_files/dburl.xml");

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        //Document doc = dBuilder.parse(is);
        Document doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();

        ////System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
        NodeList nList = doc.getElementsByTagName("DB");
        ////System.out.println("-----------------------");

        for (int i = 0; i < nList.getLength(); i++) {

            Node nNode = nList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                
                
                
                String dataBase = getTagValue("database", eElement);
                //String dbUrl = getTagValue("DataBaseURL", eElement);
                
                String dbi = dataBase;//, dbUrl);
                
                drivers.add(dbi);
            }
        }
    }
    private static String getTagValue(String sTag, Element eElement) {
	NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
 
        Node nValue = (Node) nlList.item(0);
 
	return nValue.getNodeValue();
  }
}
