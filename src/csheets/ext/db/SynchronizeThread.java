/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.db;

import csheets.core.Cell;
import csheets.core.Workbook;
import csheets.ui.ctrl.UIController;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Luis Mendes
 */
public class SynchronizeThread extends Thread{
    private UIController uiController;  
    private String typeDB; 
    private String nameTable;
    private int timeThread;
    private Cell[][] celulas;
    private String option;
    
    public SynchronizeThread(UIController ui, String type, String name, int time, Cell[][] cel,String opt){
        uiController=ui;
        timeThread=time;
        nameTable=name;
        celulas=cel;
        typeDB=type;
        option=opt;
    }
    
    
    @Override
    public void run(){
        Workbook work = uiController.getActiveWorkbook();
        //System.out.println("thread: "+typeDB);
        String [][] lastSync=null;
        while(true)
        {
            try {
                sleep(timeThread);
                System.out.println("Entrou na thread!");
                
                if (typeDB.equals("HSQLDB")){
                    SynchronizeHSQLDBLogic hsqlSync = new SynchronizeHSQLDBLogic();
                    //System.out.println(nameTable);
                    lastSync = hsqlSync.syncHSQLDB(nameTable, uiController, lastSync, option);

                } else  if (typeDB.equals("DERBY")){
                    SynchronizeDERBYLogic hsqlDERB = new SynchronizeDERBYLogic();
                    //System.out.println(nameTable);
                    lastSync = hsqlDERB.syncDERBY(nameTable, uiController, lastSync, option);
                }else{
                    System.out.println("Não definido");
                }
            
                
 
            
            } catch (InterruptedException ex) {
                Logger.getLogger(SynchronizeThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        
    }
    
}
