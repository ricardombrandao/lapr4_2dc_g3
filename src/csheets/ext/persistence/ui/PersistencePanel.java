package csheets.ext.persistence.ui;

import csheets.core.formula.compiler.FormulaCompilationException;
import csheets.io.XMLCodec;
import csheets.ui.ctrl.UIController;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Creates a new singleton panel for the sidebar with all the versions of the
 * actual loaded document
 *
 * @author Vítor Sousa
 */
public class PersistencePanel extends JPanel {

    private JList listVersions;
    private JButton btSelectVersion;
    private JButton btDeleteVersion;
//    private JButton btSaveVersion;
    private JPanel panelCentro;
    private DefaultListModel listModel;
    private UIController uiController;
    private static PersistencePanel teste = new PersistencePanel();

    /**
     * Returns the instance of this sidebar
     */
    public static PersistencePanel getInstance() {
        return teste;
    }
    
    /**
     * Creates the sidebar with its layout
     */
    private PersistencePanel() {
        super();
        setName("Persistence");
        this.setLayout(new BorderLayout());

        listModel = new DefaultListModel();
        
        listVersions = new JList(listModel);


        panelCentro = new JPanel();
        JPanel panelTop = new JPanel();
        JPanel panelDown = new JPanel();
        this.add(panelTop, BorderLayout.NORTH);
        this.add(panelDown, BorderLayout.SOUTH);
        this.add(panelCentro, BorderLayout.CENTER);

        JPanel panelList = new JPanel();
        JPanel panelGrid = new JPanel(new GridLayout(0, 1));

        JPanel panelBTLoad = new JPanel();
        JPanel panelBTDelete = new JPanel();
        JPanel panelBTSave = new JPanel();

        btSelectVersion = new JButton("Select Version");
        PersistencePanel.TrataEventSelectVersion eventSelectVersion = new PersistencePanel.TrataEventSelectVersion();
        btSelectVersion.addActionListener(eventSelectVersion);
        panelBTLoad.add(btSelectVersion);

        btDeleteVersion = new JButton("Delete Version");
        PersistencePanel.TrataEventDeleteVersion eventDeleteVersion = new PersistencePanel.TrataEventDeleteVersion();
        btDeleteVersion.addActionListener(eventDeleteVersion);
        panelBTDelete.add(btDeleteVersion);

//        btSaveVersion = new JButton("Save as actual");
//        PersistencePanel.TrataEventSaveVersion eventSaveVersion = new PersistencePanel.TrataEventSaveVersion();
//        btSaveVersion.addActionListener(eventSaveVersion);
//        panelBTSave.add(btSaveVersion);

        panelDown.add(panelGrid);
        panelCentro.add(panelList);

        panelGrid.add(panelBTLoad);
        panelGrid.add(panelBTDelete);
        panelGrid.add(panelBTSave);

        panelList.add(listVersions);


        btSelectVersion.setEnabled(false);
        btDeleteVersion.setEnabled(false);
//        btSaveVersion.setEnabled(false);


    }
    
    /**
     * Fill the sidebar with all the versions of the actual workbook
     * @param listaVal 
     */
    public void instanciaSideBar(String[] listaVal) {

        listModel.clear();

        for (int i = 0; i < listaVal.length; i++) {
            listModel.addElement(listaVal[i]);
        }

        btSelectVersion.setEnabled(true);
        btDeleteVersion.setEnabled(true);
        PersistencePanel.this.repaint();
    }

    
    public void setUIController(UIController uiController) {
        this.uiController = uiController;
    }

    
    class TrataEventSelectVersion implements ActionListener {
        /**
         * Treats the event of button Select Version.
         * It loads the selected version
         * @param event 
         */
        @Override
        public void actionPerformed(ActionEvent event) {
            if (listVersions.getSelectedValue() != null) {
                
                String id =(String) listVersions.getSelectedValue();
                
                XMLCodec xmlcod = new XMLCodec();
                xmlcod.getVersion(id, uiController.getActiveWorkbook());
            } else {
                JOptionPane.showMessageDialog(null, "No version selected.\nPlease, choose one.");
            }

        }
    }

    class TrataEventDeleteVersion implements ActionListener {
        /**
         * Treats the event of button Delete Version.
         * It loads the selected version
         * @param event 
         */
        @Override
        public void actionPerformed(ActionEvent event) {
            if (listVersions.getSelectedValue() != null) {
                String id = (String) listVersions.getSelectedValue();

                listModel.remove(listVersions.getSelectedIndex());

//                btSaveVersion.setEnabled(true);
                XMLCodec xmlcod = new XMLCodec();
                xmlcod.deleteVersion(id);
                try {
                    uiController.getActiveSpreadsheet().getCell(0,0).setContent(uiController.getActiveSpreadsheet().getCell(0,0).getContent() + " ");
                } catch (FormulaCompilationException ex) {
                    Logger.getLogger(PersistencePanel.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }else{
                JOptionPane.showMessageDialog(null, "No version selected.\nPlease choose one.");
            }
        }
    }

    /*class TrataEventSaveVersion implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
        }
    }*/
}