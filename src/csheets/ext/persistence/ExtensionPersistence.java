package csheets.ext.persistence;

import csheets.ext.Extension;
import csheets.ext.persistence.ui.UIExtensionPersistence;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 *
 * @author Vítor
 */
public class ExtensionPersistence extends Extension{
    /** The name of the extension */
	public static final String NAME = "Persistence";

	/**
	 * Creates a new persistence extension.
	 */
	public ExtensionPersistence() {
		super(NAME);
	}
	
	/**
	 * Returns the user interface extension of this extension (an instance of the class {
         * @link  csheets.ext.persistence.ui.UIExtensionPersistence}). <br/>
	 * In this extension only extends the user interface.
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionPersistence(this, uiController);
	}
}
