/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package csheets.ext.server.ui;

import csheets.core.Cell;
import csheets.ui.ctrl.FocusOwnerAction;
import java.awt.event.ActionEvent;

/**
 *
 * @author Vítor
 */
public class FocusOwnerServer extends FocusOwnerAction {
    private Cell[][] cells;
    
    public FocusOwnerServer(){
        cells = focusOwner.getSelectedCells();
    }
    
    public Cell[][] getSelectedCells(){
        return cells;
    }
    
    
    
    @Override
    protected String getName() {
        return "FocusOwnerServer";
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
