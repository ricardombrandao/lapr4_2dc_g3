package csheets.ext.server.ui;

import java.awt.event.KeyEvent;

import javax.swing.JMenu;

import csheets.ui.ctrl.UIController;

/**
 * Representes the UI extension menu of the server.
 * @author Vítor Sousa
 */
public class ServerMenu extends JMenu {

	/**
	 * Creates a server menu button.
	 * This constructor creates and adds the menu options. 
	 * A menu option is an action (in this case {@link csheets.ext.server.ui.ServerAction})
	 * @param uiController the user interface controller
	 */
	public ServerMenu(UIController uiController) {
		super("Server");
		setMnemonic(KeyEvent.VK_V);

		// Adds font actions
		add(new ServerAction(uiController));
	}	
}
