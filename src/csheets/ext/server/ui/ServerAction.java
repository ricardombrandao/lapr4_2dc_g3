package csheets.ext.server.ui;

import csheets.core.Cell;
import csheets.ext.server.*;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * This action will copy the selected cells on spreadsheet to a socket, which 
 * allows another instance to get them.
 *
 * @author Vítor Sousa
 */
public class ServerAction extends FocusOwnerAction {

    /**
     * The user interface controller
     */
    protected UIController uiController;
    /**
     * The port in wich cells will be putted
     */
    private int port;
    /**
     * Class variable to control the selected cells
     */
    private String[][] cells;

    /**
     * Creates a new action.
     *
     * @param uiController the user interface controller
     */
    public ServerAction(UIController uiController) {
        this.uiController = uiController;
    }

    protected String getName() {
        return "Share Cells";
    }

    public int getPort() {
        return port;
    }

    protected void defineProperties() {
    }

    /**
     * A server action that presents a input dialog, so user can choose which port
     * to put the information. After that, user receives an information with his own
     * IP and the port he has choosen
     *
     * @param event the event that was fired
     */
    public void actionPerformed(ActionEvent event) {
        try {
            //Ask for a port value
            String str = JOptionPane.showInputDialog("Please choose a port (between 20000 and 60000)", null);
            //if the user didn't hit cancel button, check the validity of the insert
            if (str != null) {
                port = Integer.parseInt(str);
                if (port < 20000 || port > 60000) {
                   throw new Exception();
                }
            
                //Inform user which IP he owns and which port he has choosen
                JOptionPane.showMessageDialog(null, "IP: "+
                        InetAddress.getLocalHost().getHostAddress()+"\nPort: "+ port);
                
                //Get the cells previously selected
                Cell[][] tCells = focusOwner.getSelectedCells();
                cells = new String[tCells.length][tCells[0].length];
                //Convert to string
                for (int i = 0; i < tCells.length; ++i){
                    for(int j = 0; j < tCells[i].length; ++j){
                        System.out.println(tCells[i][j].getContent());
                        cells[i][j] = tCells[i][j].getContent();
                    }
                }
                //Pass it to controller
                ServerController serverC = new ServerController(port, cells);
                serverC.shareCells();
            }

        } catch (Exception ex) {
            //Inform user that the value is not correct and ask it again
            JOptionPane.showMessageDialog(null, "Please enter a value between 20000 and 60000");
            actionPerformed(event);
        }
    }
}
