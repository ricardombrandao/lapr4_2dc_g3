package csheets.ext.server.ui;

import csheets.core.Cell;
import csheets.ext.server.ServerController;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Creates a new panel for the sidebar with user's IP
 * and a text box so user can choose in which port pretends 
 * to create the connection
 * 
 * @author Vítor Sousa
 */
public class ServerPanel extends JPanel{
    private int port;
    private String[][] cells = null;
    
    private JTextField txPorta = new JTextField(5);
    
    public ServerPanel() {
        super();
        setName("Share");
        this.setLayout(new BorderLayout());
        
        
        JLabel lbPorta = new JLabel("Port: ");
        JButton btShare = new JButton("Share");
        JLabel lbIP = new JLabel("IP: ");
        JLabel lbIPShow;
        
        try {
            lbIPShow = new JLabel(InetAddress.getLocalHost().getHostAddress());
        } catch (Exception e) {
            lbIPShow = new JLabel("No IP");
        }
                
        ServerPanel.TrataEventoBtShare eventoBTShare = new ServerPanel.TrataEventoBtShare();
        btShare.addActionListener(eventoBTShare); 
        
        JPanel painelGrid = new JPanel(new GridLayout(0, 1));
        JPanel painelDown = new JPanel();
        JPanel painelIP = new JPanel();
        JPanel painelPorta = new JPanel();
        
        painelIP.add(lbIP);
        painelIP.add(lbIPShow);
        painelPorta.add(lbPorta);
        painelPorta.add(txPorta);
        
        painelGrid.add(painelIP);
        painelGrid.add(painelPorta);

        painelDown.add(btShare);

        this.add(painelGrid, BorderLayout.CENTER);
        this.add(painelDown, BorderLayout.SOUTH);
    }

    class TrataEventoBtShare implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
           JOptionPane.showMessageDialog(null, "Not implemented yet.\n"
                    + "Please choose Extensions->Server->Share Cells");
        }
    }
}