package csheets.ext.server;

/**
 * A controller to share the selected cells.
 * This class creates a new thread and starts it.
 * @author Vítor Sousa
 */
public class ServerController{
    private int port;
    private String[][] cells;
    
    
    public ServerController(int port, String[][] cells){
        this.port=port;
        this.cells = cells;
    }
    
    
    public void shareCells(){
        ServerShare serv = new ServerShare(port, cells);
        Thread t = new Thread(serv);
        t.start();
    }
}
