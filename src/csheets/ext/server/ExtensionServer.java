package csheets.ext.server;

import csheets.ext.Extension;
import csheets.ext.server.ui.UIExtensionServer;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * An extension to create a server that will allow to share cell with another programm's instance.
 * The class that implements the Extension is the "bootstrap" of the extension.
 * @see Extension
 * @author Vítor Sousa
 */
public class ExtensionServer extends Extension {

	/** The name of the extension */
	public static final String NAME = "Server";

	/**
	 * Creates a new Server extension.
	 */
	public ExtensionServer() {
		super(NAME);
	}
	
	/**
	 * Returns the user interface extension of this extension (an instance of the class {@link  csheets.ext.server.ui.UIExtensionServer}). <br/>
	 * In this extension server only extends the user interface.
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionServer(this, uiController);
	}
}
