package csheets.ext.server;

import csheets.core.Cell;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import javax.swing.JOptionPane;
import java.net.*;

/**
 * This class is responsible for creating a socket 
 * that will share the selected cells, through the Runnable method run()
 * @author Vítor
 */
public class ServerShare implements Runnable {

    private int port;
    private String[][] cells;

    public ServerShare(int port, String[][] cells) {
        this.port = port;
        this.cells = cells;
    }

    @Override
    public void run() {
        ServerSocket servSocket;
        Socket socket = null;
        OutputStream output = null;
        try {
            servSocket = new ServerSocket(port);
            socket = servSocket.accept();
            output = socket.getOutputStream();
            ObjectOutputStream objectOutput = new ObjectOutputStream(output);
            objectOutput.writeObject(cells);
            
            JOptionPane.showMessageDialog(null, "Cells shared!");
            servSocket.close();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Port not available. "
                    + "Please choose another one.\nPort: " + port);
        } finally {
            try {
                if(output != null)
                    output.close();
                if(socket != null)
                    socket.close();
            } catch (IOException ex) {
                System.out.println("Exception!");
            }
        }
    }
}
