package csheets.ext.client;

import csheets.core.Cell;

/**
 * A controller to receive the shared cells.
 * This class creates a new thread and starts it.
 * It also receive the information from ClientReceive and returns it to 
 * ClientAction, to be written on spreadsheet
 * @author Vítor Sousa
 */
public class ClientController {
    private int port;
    private int row, col;
    private String ip;

    public ClientController(String ip, int port, int row, int col) {
        this.port = port;
        this.row = row;
        this.col = col;
        this.ip = ip;
    }
    
    public String[][] receiveCells() throws InterruptedException {
        ClientReceive serv = new ClientReceive(ip, port);
        Thread t = new Thread(serv);
        t.start();
        //Waits a short time, so the socket has time to write information on cells
        t.sleep(200);
        return serv.getCells();        
    }
}
