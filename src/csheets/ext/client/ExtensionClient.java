package csheets.ext.client;

import csheets.ext.Extension;
import csheets.ext.client.ui.UIExtensionClient;
import csheets.ui.ctrl.UIController;
import csheets.ui.ext.UIExtension;

/**
 * An extension to create a client that will allow to receive cells from another
 * application's instance.
 * An extension must extend the Extension abstract class.
 * The class that implements the Extension is the "bootstrap" of the extension.
 * @see Extension
 * @author Vítor Sousa
 */
public class ExtensionClient extends Extension {

	/** The name of the extension */
	public static final String NAME = "Client";

	/**
	 * Creates a new Server extension.
	 */
	public ExtensionClient() {
		super(NAME);
	}
	
	/**
	 * Returns the user interface extension of this extension (an instance of the class {@link  csheets.ext.client.ui.UIExtensionServer}). <br/>
	 * In this extension server only extends the user interface.
	 * @param uiController the user interface controller
	 * @return a user interface extension, or null if none is provided
	 */
	public UIExtension getUIExtension(UIController uiController) {
		return new UIExtensionClient(this, uiController);
	}
}
