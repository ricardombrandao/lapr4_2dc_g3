package csheets.ext.client;

import csheets.core.*;
import java.io.*;
import java.net.*;
import javax.swing.JOptionPane;

/**
 * This class implements Runnable.
 * It reads the information that's in socket and returns it to the controller
 * @author Vítor Sousa
 */
public class ClientReceive implements Runnable {

    private String ip;
    private int port;
    private Socket clientSocket;
    private String[][] cells;

    public ClientReceive(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public String[][] getCells() {
        return cells;
    }
    /*
     * Creates and connects to a new socket
     * and reads its cells and returns it.
     */
    @Override
    public void run() {
        try {
            
            clientSocket = new Socket(ip, port);
            System.out.println("Socket criado");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "IP or port not available");
        }

        try {
            InputStream inSocketStream = clientSocket.getInputStream();
            System.out.println("InputStream criado");
            ObjectInputStream in = new ObjectInputStream(inSocketStream);
            System.out.println("Inicializado");
            cells = (String[][]) in.readObject();
            System.out.println("Objectos lidos");
        
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "An error occurred when getting"
                    + "the cells");
        }
    }

}
