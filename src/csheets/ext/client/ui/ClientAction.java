package csheets.ext.client.ui;

import csheets.core.Cell;
import csheets.core.Spreadsheet;
import csheets.ext.client.*;
import csheets.ui.ctrl.FocusOwnerAction;
import csheets.ui.ctrl.UIController;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 * An action of the client, which will start the whole process of getting
 * cells from a socket
 *
 * @author Vítor Sousa
 */
public class ClientAction extends FocusOwnerAction {

    static private final String IPV4_REGEX = "(([0-1]?[0-9]{1,2}.)|(2[0-4][0-9].)|(25[0-5].)){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))";
    static private Pattern IPV4_PATTERN = Pattern.compile(IPV4_REGEX);
    /**
     * The user interface controller
     */
    protected UIController uiController;
    private int port;
    private String ip;
    private int row, col;
    private String[][] cells;

    /**
     * Creates a new action.
     *
     * @param uiController the user interface controller
     */
    public ClientAction(UIController uiController) {
        this.uiController = uiController;
    }

    protected String getName() {
        return "Receive Cells";
    }

    public int getPort() {
        return port;
    }

    protected void defineProperties() {
    }

    private boolean validIP(String ip) {
        return IPV4_PATTERN.matcher(ip).matches();
    }

    /**
     * A client action that presents two input dialog. The user will choose
     * which IP he chooses to receive from and in which port.
     *
     * @param event the event that was fired
     */
    public void actionPerformed(ActionEvent event) {
        //Each time it selects receive, user must introduce ip and port
        ip = "";
        try {
            row = focusOwner.getSelectedRow();
            col = focusOwner.getSelectedColumn();

            if (ip.equals("") || ip == null) {
                ip = JOptionPane.showInputDialog("Please insert ip", null);

                if (ip != null) {
                    if (!validIP(ip)) {
                        ip = "";
                    }
                    String str = JOptionPane.showInputDialog("Please choose a port (between 20000 and 60000)", null);
                    //if the user didn't hit cancel button, check the validity of the insert
                    if (str != null) {
                        port = Integer.parseInt(str);
                        if (port < 20000 || port > 60000) {
                            //throw new Exception();
                        }


                        ClientController client = new ClientController(ip, port, row, col);
                        try {
                            cells = client.receiveCells();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(ClientAction.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        if (cells == null) {
                            throw new IOException();
                            
                        }

                        for (int i = 0; i < cells.length; ++i) {
                            for (int j = 0; j < cells[i].length; ++j) {
                                System.out.print(cells[i][j] + "\t");
                                focusOwner.getSpreadsheet().getCell(col + j, row + i).setContent(cells[i][j]);
                            }

                        }
                    }
                }
            }
        }catch(IOException ex){
            JOptionPane.showMessageDialog(null, "No data to receive");
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Incorrect format for IP.\n"
                    + "Please insert another one.");
            actionPerformed(event);
        }

    }
}
