package csheets.ext.client.ui;

import java.awt.event.KeyEvent;

import javax.swing.JMenu;

import csheets.ui.ctrl.UIController;

/**
 * Representes the UI extension menu of the client.
 * @author Vítor Sousa
 */
public class ClientMenu extends JMenu {

	/**
	 * Creates a client menu button.
	 * This constructor creates and adds the menu options. 
	 * A menu option is an action (in this case {@link csheets.ext.client.ui.ClientAction})
	 * @param uiController the user interface controller
	 */
	public ClientMenu(UIController uiController) {
		super("Client");
		setMnemonic(KeyEvent.VK_C);

		// Adds font actions
		add(new ClientAction(uiController));
	}	
}
