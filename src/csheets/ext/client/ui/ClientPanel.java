package csheets.ext.client.ui;

import csheets.core.Cell;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Creates a new panel for the sidebar with two textboxes
 * so user can choose in which port pretends to create the connection and
 * and in which IP.
 * 
 * @author Vítor Sousa
 */
public class ClientPanel extends JPanel {

    private int port;
    private String ip;
    private String[][] cells;

    public ClientPanel() {
        super();
        setName("Receive");
        
        this.setLayout(new BorderLayout());
        
        JLabel lbIP = new JLabel("IP: ");
        
        JTextField txIP = new JTextField(10);
        JLabel lbPorta = new JLabel("Port: ");

        JTextField txPorta = new JTextField(5);
        JButton btShare = new JButton("Receive");
        ClientPanel.TrataEventoBtShare eventoBTShare = new ClientPanel.TrataEventoBtShare();
        btShare.addActionListener(eventoBTShare);

        JPanel painelGrid = new JPanel(new GridLayout(0, 1));
        JPanel painelDown = new JPanel();
        
        JPanel painelIP = new JPanel();
        painelIP.add(lbIP);
        painelIP.add(txIP);
        
        JPanel painelPorta = new JPanel();
        painelPorta.add(lbPorta);
        painelPorta.add(txPorta);
        
        painelGrid.add(painelIP);
        painelGrid.add(painelPorta);

       
        painelDown.add(btShare);

        this.add(painelGrid, BorderLayout.CENTER);
        this.add(painelDown, BorderLayout.SOUTH);
    }

    class TrataEventoBtShare implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            JOptionPane.showMessageDialog(null, "Not implemented yet.\n"
                    + "Please choose Extensions->Client->Receive Cells");
        }
    }
}