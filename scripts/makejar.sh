#!/bin/sh
echo Compiling...
./build.sh

echo Copying resource files...
cp -R ../src-resources/csheets ../tmp-build 

echo Creating archive...
jar cmf makejar.mf ../dist/csheets.jar -C ../tmp-build csheets

echo Copiar as dependencias
cp ../lib/antlr.jar ../dist/antlr.jar
cp ../lib/dbunit-2.4.9.jar ../dist/dbunit-2.4.9.jar
cp ../lib/derby.jar ../dist/derby.jar
cp ../lib/hsqldb.jar ../dist/hsqldb.jar
cp ../lib/junit-4.10.jar ../dist/junit-4.10.jar
cp ../lib/slf4j-api-1.7.5.jar ../dist/slf4j-api-1.7.5.jar
cp ../lib/slf4j-nop-1.7.5.jar ../dist/slf4j-nop-1.7.5.jar


# echo Removing temporary files...
# rm -R jar

pause