@echo off

echo Compiling...
call build

echo Copying temporary files...
call xcopy ..\src-resources\csheets ..\tmp-build\csheets /S /Q /Y > nul

echo Creating archive...
call jar cmf makejar.mf ..\dist\csheets.jar -C ..\tmp-build csheets

echo Copiar as dependencias
call copy /Y ..\lib\antlr.jar ..\dist 
call copy /Y ..\lib\dbunit-2.4.9.jar ..\dist 
call copy /Y ..\lib\derby.jar ..\dist 
call copy /Y ..\lib\hsqldb.jar ..\dist 
call copy /Y ..\lib\junit-4.10.jar ..\dist 
call copy /Y ..\lib\slf4j-api-1.7.5.jar ..\dist 
call copy /Y ..\lib\slf4j-nop-1.7.5.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\org-apache-commons-logging.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\jta.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\jdbc2_0-stdext.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\javassist.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\hibernate-tools.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\hibernate-entitymanager.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\hibernate-commons-annotations.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\hibernate-annotations.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\hibernate3.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\ejb3-persistence.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\ehcache-1.2.3.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\dom4j-1.6.1.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\commons-collections-2.1.1.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\cglib-2.1.3.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\asm-attrs.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\asm.jar ..\dist 
call copy /Y ..\lib\hibernate-persistence\antlr-2.7.6.jar ..\dist 
call copy /Y ..\lib\javax.persistence.jar ..\dist 


REM echo Removing temporary files...
REM call rmdir jar /Q /S
